import * as xpath from './xpath.js';
import { Utils } from './utilities.js';
import { Items } from './itemManager.js';

export class Bundles {
    constructor(node, sortNumber, exportPack, pack, folder, context, options = {}) {
        this.context = context;
        this.sortNumber = sortNumber;
        this.importData = context.importData;
        this.node = node;
        this.folder = folder;
        this.exportPack = exportPack;
        this.pack = pack;
        this.options = options;
    }

    /**
     *
     * @param {*} node
     * @param {*} context
     * @param {*} exportPack
     * @param {*} pack
     * @param {*} folder
     * @returns
     */
    async processBundleItems(node) {
        // console.log('bundleManager.js processBundleItems', { node });
        let itemIdList = [];

        let nodeList = xpath.getNodeList(node, `itemlist/*`);
        // let packList = {};
        // let folderList = {};
        let itemType = 'item';

        for (const node of nodeList) {
            itemType = 'item';
            const nodeType = xpath.getNodeValue(node, 'type').toLowerCase();
            const nodeSubType = xpath.getNodeValue(node, 'subtype').toLowerCase();
            itemType = Utils.getItemType(nodeType, nodeSubType);

            const itemInfo = await new Items(node, itemType, this.exportPack, this.pack, this.folder, this.context).import(
                node,
                itemType
            );
            // itemIdList.push({ ...itemInfo, count: itemInfo.system.quantity });
            itemIdList.push(itemInfo);
        }

        // console.log(`Done importing ${nodeList.length} items into ${itemType} compendium for bundle.`);
        return itemIdList;
    }

    //TODO: need to figure out what to do with these in bundles
    async processCurrencyItems(coinsList) {
        const currencyIDList = [];
        const variant = parseInt(game.ars.config.settings.systemVariant) ?? 0;
        for (const [coinType, quantity] of Object.entries(coinsList)) {
            if (quantity) {
                const currencyName =
                    game.i18n.localize(`ARS.currency.short.${coinType}`) + ` ${game.i18n.localize('ARS.coins')}` || 'Change';
                let coinItem = game.items.getName(currencyName);
                if (!coinItem) {
                    const currencyWeight = 1 / game.ars.config.currencyWeight[variant];
                    const createdCoin = await Item.create({
                        name: currencyName,
                        type: 'currency',
                        img: game.ars.config.icons.general.currency[coinType],
                        system: {
                            weight: currencyWeight,
                            cost: {
                                currency: coinType,
                            },
                        },
                    });
                    coinItem = createdCoin;
                }
                if (coinItem) {
                    currencyIDList.push({
                        uuid: coinItem.uuid,
                        id: coinItem.id,
                        count: quantity,
                        img: coinItem.img,
                        name: coinItem.name,
                        type: 'currency',
                    });
                }
            }
        }

        return currencyIDList;
    }
    /**
     *
     * @param {*} node
     * @param {*} bundleItemFolder
     */
    async import(node, bundleItemFolder) {
        // console.log(`Bundle importing `, { node }, this);
        const name = xpath.getNodeValue(node, 'name', 'UNKNOWN');
        const description = Utils.reformatMarkup(xpath.getNodeValue(node, 'description'), this.importData, 200);

        let currency = [];
        let coinlist = xpath.getNodeList(node, 'coinlist/*');
        for (const nodeCoin of coinlist) {
            let coinName = xpath.getNodeValue(nodeCoin, 'description');
            coinName = coinName.toLowerCase();
            const coinAmount = parseInt(xpath.getNodeValue(nodeCoin, 'amount', 0));
            currency[coinName] = coinAmount;
        }

        const bundleIdList = await this.processBundleItems(node);
        const currencyIdList = await this.processCurrencyItems(currency);
        // Combine into a new list
        const combinedList = [...bundleIdList, ...currencyIdList];

        let bundleItem = await Item.create(
            {
                type: 'bundle',
                name: Utils.cleanSortingName(name),
                // img: 'icons/containers/chest/chest-reinforced-steel-walnut-brown.webp',
                img: game.ars.config.icons.general.items['bundle'],
                folder: this.folder.id,
                sort: this.sortNumber,
                system: {
                    itemList: combinedList,
                    description: description,
                    currency: {
                        ...currency,
                    },
                },
            },
            { displaySheet: false }
        );

        this.context.maps.bundles.set(node.nodeName, bundleItem);
        // console.log(`Done importing ${bundleItem.name}`);

        if (!this.importData.bundleImported) this.importData.bundleImported = [];
        this.importData.bundleImported.push({
            nodeId: node.nodeName,
            type: 'bundle',
            name: bundleItem.name,
            id: bundleItem.id,
            img: bundleItem.img,
        });
    }
} // end class Bundles
