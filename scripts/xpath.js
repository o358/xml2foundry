export function* queryXPathAll(node, expr) {
    if (node) {
        const evaluator = new XPathEvaluator();
        const document = node?.ownerDocument?.documentElement ?? node.documentElement;
        const resolver = evaluator.createNSResolver(document);
        const results = evaluator.evaluate(expr, node, resolver, 0, null);
        let result;
        while ((result = results.iterateNext())) {
            yield result;
        }
    } else {
        console.log('node is null');
    }
}

export function queryXPath(node, expr) {
    for (const result of queryXPathAll(node, expr)) {
        return result;
    }
}

/**
 *
 * Get value of a node
 *
 * @param {Object} node xPath node
 * @param {String} valueName  nodeName to get value for
 * @param {String} defaultValue returned if value not found
 * @returns
 */
export function getNodeValue(node, valueName, defaultValue = '') {
    return queryXPath(node, valueName)?.innerHTML || defaultValue;
}

/**
 *
 * This deals with path/category entries that FG has sometimes, sometimes not.
 *
 * @param {String (XML content)} data
 * @param {String} nodePath
 * @returns
 */
export function getNodeList(data, nodePath) {
    let nodes = [];
    for (const dataNode of queryXPathAll(data, nodePath)) {
        const _categoryNodes = (data) => {
            let nodeList = [];
            for (const npcCatNode of queryXPathAll(data, '*')) {
                nodeList.push(npcCatNode);
            }
            return nodeList;
        };

        if (dataNode.nodeName === 'category') {
            nodes = nodes.concat(_categoryNodes(dataNode));
        } else {
            nodes.push(dataNode);
        }
    }
    return nodes;
}

// access attributes of an XML or HTML element in the DOM.
export function getAttributeType(node, attributeName) {
    return node.getAttribute(attributeName) || '';
}

export function nodeListExist(node, expr) {
    const evaluator = new XPathEvaluator();
    const document = node.ownerDocument?.documentElement ?? node.documentElement;
    const resolver = evaluator.createNSResolver(document);
    const result = evaluator.evaluate(expr, node, resolver, XPathResult.ANY_TYPE, null);

    // Check if there is at least one node that matches the expression
    return result.iterateNext() !== null;
}
