import * as xpath from './xpath.js';
import { Utils } from './utilities.js';
import { Scenes } from './sceneManager.js';
import { Items } from './itemManager.js';
import { Bundles } from './bundleManager.js';
import { Tables } from './tableManager.js';
import { Actors } from './actorManager.js';
import { Journals } from './journalManager.js';
import { Encounters } from './encounterManager.js';
import { JournalManuals } from './journalManuals.js';

// Set up the user interface
Hooks.on('renderSidebarTab', async (app, html) => {
    if (app.options.id == 'compendium') {
        let button2 = $(
            "<button class='import-xml' title='Select db.xml file in campaign folder.\nMust reside under Foundry data/ Directory'><i class='fas fa-file-import'></i>XML Import</button>"
        );

        button2.click(function () {
            selectXmlFile(this);
        });

        html.find('.directory-footer').append(button2);
    }
});

/**
 *
 * Prompt for XML file and handoff to parse/import functions
 *
 * @param {*} event
 */
async function selectXmlFile(event) {
    function parseFilePath(filePath) {
        // Find the last index of '/'
        const lastSlashIndex = filePath.lastIndexOf('/');

        // Extract the directory path (immediate parent directory only)
        const directoryPath = filePath.substring(0, lastSlashIndex);
        const directory = directoryPath.substring(directoryPath.lastIndexOf('/') + 1);

        // Extract the filename without extension
        const fileNameWithExtension = filePath.substring(lastSlashIndex + 1);
        const fileName = fileNameWithExtension.replace('.xml', '');

        return [directory, fileName];
    }

    let current;
    let theFile = await new Promise((resolve) => {
        const fp = new FilePicker({
            current: current,
            callback: (path) => {
                current = path;
                resolve(path);
            },
        });
        fp.extensions = ['.xml'];
        fp.browse(current);
    });
    console.log('theFile--->', theFile);
    let inputXML = await new Promise((resolve) => {
        fetch(theFile)
            .then((response) => response.text())
            .then((data) => {
                resolve(data);
            });
    });

    let [importPath, importName] = parseFilePath(current);

    const match = current.match(/^(.*)\/(.*)\/.*\.xml$/i);

    // importName = decodeURI(importName);
    importName = decodeURI(importPath);

    let folderData = {
        name: importName,
        type: 'Item',
        sorting: 'a',
        sort: 0,
    };
    const itemRootFolder = await Folder.implementation.create(folderData);
    // const itemRootFolder = await Utils.getFolder(folderData.name, folderData);

    const importData = {
        imported: {},
        importPath: importPath,
        importName: importName,
        itemFolderRoot: itemRootFolder,
    };

    // XmlImporter.parseXml(inputXML, importData);
    const importObject = new XmlImporter(inputXML, importData);
    importObject.parseXml();
}

/**
 * Import functions
 */
class XmlImporter {
    constructor(inputXML, importData, options = {}) {
        this.inputXML = inputXML;
        this.importData = importData;
        this.options = options;

        this.maps = {
            scenes: new Map(),
            npcs: new Map(),
            journals: new Map(),
            items: new Map(),
            tables: new Map(),
            encounters: new Map(),
            bundles: new Map(),
        };
    }
    /**
     *
     * Process XML file and import npc, items, spells, etc
     *
     */

    async parseXml() {
        // function checkPathExistence(data, path) {
        //     const evaluator = new XPathEvaluator();
        //     const resolver = evaluator.createNSResolver(data.ownerDocument?.documentElement ?? data.documentElement);
        //     const result = evaluator.evaluate(path, data, resolver, XPathResult.ANY_TYPE, null);
        //     return result.iterateNext() !== null;
        // }
        const data = new DOMParser().parseFromString(this.inputXML, 'text/xml');
        this.data = data;

        // game.ars.library.isImporting = true;

        console.log('parseXml data ==>', data);

        // use this in various locations for mapping to image bitmaps
        this.importData.imgNodeList = xpath.getNodeList(data, '/root/image/*');
        this.importData.npcNodeList = xpath.getNodeList(data, '/root/npc/*');
        this.importData.tableNodeList = xpath.getNodeList(data, '/root/tables/*');
        this.importData.spellNodeList = xpath.getNodeList(data, '/root/spell/*');
        this.importData.itemNodeList = xpath.getNodeList(data, '/root/item/*');

        this.importData.chapterList = xpath.getNodeList(data, '/root/reference/refmanualindex/chapters/*');

        // let manualsPath;
        // if (checkPathExistence(data, '/root/reference/refmanualindex')) {
        //     manualsPath = '/root/reference/refmanualindex/chapters/*';
        // } else if (checkPathExistence(data, '/root/refmanualindex')) {
        //     manualsPath = '/root/refmanualindex/chapters/*';
        // }

        // if (manualsPath) {
        //     this.importData.chapterList = xpath.getNodeList(data, manualsPath);
        // } else {
        //     console.error('No Manual valid path found');
        //     this.importData.chapterList = []; // or handle this scenario as needed
        // }
        // this.importData.chapterList = xpath.getNodeList(data, '/root/reference/refmanualindex/chapters/*');

        console.log('parseXml importData ==>', JSON.stringify(this.importData));
        /**
         *
         * Process NPCs in the XML file
         *
         */
        async function processNPCs(export_type = 'compendium') {
            const exportPack = export_type === 'compendium';
            const actorFolder = exportPack
                ? null
                : await Utils.createFolder(`${this.importData.importName}-actor`, 'Actor', 'a');
            const actorPack = await Utils.getComp(`${this.importData.importName}-actor`, 'Actor');
            // deal with categories of npcs, build array of all npcs
            let nodeList = xpath.getNodeList(data, '/root/npc/*');

            nodeList.sort(Utils.sortNodeByName);

            // now process all NPCs
            let sortNumber = 0;
            for (const node of nodeList) {
                sortNumber += 1;
                const actorObject = new Actors(node, exportPack, actorPack, actorFolder, this);
                await actorObject.import(node, sortNumber);
            }
            // if (nodeList.length) ui.notifications.warn(`Done importing ${nodeList.length + 1} NPCs`);
        }

        /**
         *
         */
        async function processJournals() {
            let sortNum = 0;
            let folderSortNum = 0;

            const folderData = {
                name: this.importData.importName,
                type: 'JournalEntry',
                sorting: 'm',
                sort: 0,
            };
            const journalFolder = await Folder.implementation.create(folderData);
            const rootChapter = journalFolder;
            let lastChapter = rootChapter;
            let lastSubChapter = undefined;

            const tagData = {
                chapter: 0,
                sub: 1,
            };

            async function _createFolder(subName, newChapter = false) {
                folderSortNum += 1;
                const chapterFolderData = {
                    name: Utils.cleanSortingName(subName),
                    type: 'JournalEntry',
                    sorting: 'm',
                    sort: folderSortNum,
                    parent: newChapter ? rootChapter.id : lastChapter.id,
                };
                const chapter = await Folder.implementation.create(chapterFolderData);
                if (newChapter) {
                    lastSubChapter = undefined;
                    tagData.chapter++;
                    tagData.sub = 0;
                } else {
                    tagData.sub++;
                }
                return chapter;
            }

            // import the journal entry
            async function importJournal(importNode) {
                // while importing look for subchapters (from author, <subchapter>)
                const subChapter = parseInt(xpath.getNodeValue(importNode, 'subchapter', '0'));
                if (subChapter) {
                    const storyName = xpath.getNodeValue(importNode, 'name', 'UNKNOWN-SUBCHAPTER');
                    lastSubChapter = await _createFolder(storyName, false);
                }
                const putInFolder = lastSubChapter ? lastSubChapter : lastChapter;
                sortNum += 1;
                await new Journals(importNode, putInFolder, this).import(importNode, sortNum);
            }

            for (const dataNode of xpath.queryXPathAll(data, `/root/encounter/*`)) {
                const _categoryNodes = (catNodes) => {
                    let nodeList = [];
                    for (const catNode of xpath.queryXPathAll(catNodes, '*')) {
                        nodeList.push(catNode);
                    }
                    return nodeList;
                };
                // check if node is a category, if so make new chapter and import into it.
                if (dataNode.nodeName === 'category') {
                    const newChapter = dataNode.getAttribute('name') || 'NEW-CHAPTER-NAME';
                    lastChapter = await _createFolder(newChapter, true);

                    const chapterNodes = _categoryNodes(dataNode);
                    chapterNodes.sort(Utils.sortNodeByName);
                    for (const chapterNode of chapterNodes) {
                        await importJournal.bind(this)(chapterNode);
                    }
                } else {
                    await importJournal.bind(this)(dataNode);
                }
            }
        }
        /**
         * Import item/*
         */
        async function processItems(export_type = 'compendium', path = '/root/item/', staticType) {
            const exportPack = export_type === 'compendium';
            let skillsList = [];
            let nodeList = xpath.getNodeList(data, `${path}/*`);
            const pack = await Utils.getComp(`${this.importData.importName}-item`, 'Item');
            const folder = exportPack ? null : await Utils.createFolder(`${this.importData.importName}-item`, 'Item', 'a');
            let itemType = 'item';

            for (const node of nodeList) {
                itemType = 'item';

                if (!staticType) {
                    const nodeType = xpath.getNodeValue(node, 'type').toLowerCase();
                    const nodeSubType = xpath.getNodeValue(node, 'subtype').toLowerCase();
                    itemType = Utils.getItemType(nodeType, nodeSubType);
                } else {
                    itemType = staticType;
                }

                const itemObject = new Items(node, itemType, exportPack, pack, folder, this);
                const itemInfo = await itemObject.import(node, itemType);

                if (itemType === 'skill' && itemInfo) skillsList.push(itemInfo);
                console.log(`Imported item ${itemInfo?.name}`);
            }
            // if (nodeList.length) ui.notifications.warn(`Done importing ${nodeList.length} items`);
            return skillsList;
        }

        async function processTables(export_type) {
            const exportPack = export_type === 'compendium';
            console.log('parseXml processTables START');
            const pack = await Utils.getComp(`${this.importData.importName}-table`, 'RollTable');
            let folder = exportPack ? null : await Utils.createFolder(`${this.importData.importName}-table`, 'RollTable');
            let nodeList = xpath.getNodeList(data, '/root/tables/*');
            nodeList.sort(Utils.sortNodeByName);
            let sortNumber = 0;
            for (const node of nodeList) {
                sortNumber += 1;
                const tableObject = new Tables(node, exportPack, pack, folder, this);
                await tableObject.import(node, sortNumber);
            }
            // if (nodeList.length) ui.notifications.warn(`Done importing ${nodeList.length + 1} tables`);
        }

        async function processBundles(export_type) {
            const exportPack = export_type === 'compendium';
            console.log('processBundles START');
            const pack = await Utils.getComp(`${this.importData.importName}-Bundle`, 'Item');

            let folderData = {
                name: 'Bundles',
                type: 'Item',
                sorting: 'm',
                parent: this.importData.itemFolderRoot.id,
            };
            const bundleFolder = await Utils.getFolder(folderData.name, folderData);

            folderData = {
                name: `Items`,
                type: 'Item',
                sorting: 'a',
                parent: bundleFolder.id,
            };
            const bundleItemFolder = await Utils.getFolder(folderData.name, folderData);

            let nodeList = xpath.getNodeList(data, '/root/treasureparcels/*');
            nodeList.sort(Utils.sortNodeByName);
            let sortNumber = 0;
            for (const node of nodeList) {
                sortNumber += 1;
                await new Bundles(node, sortNumber, exportPack, pack, bundleFolder, this).import(node, bundleItemFolder);
            }
            // if (nodeList.length) ui.notifications.warn(`Done importing ${nodeList.length + 1} bundles`);
        }

        async function processEncounters(battleType = 'battle') {
            console.log('processEncounters START');
            let folderData = {
                name: 'Encounters',
                type: 'Item',
                sorting: 'm',
                parent: this.importData.itemFolderRoot.id,
            };
            const encounterFolder = await Utils.getFolder(folderData.name, folderData);

            let nodeList = xpath.getNodeList(data, `/root/${battleType}/*`);
            nodeList.sort(Utils.sortNodeByName);
            let sortNumber = 0;
            for (const node of nodeList) {
                sortNumber += 1;
                await new Encounters(node, encounterFolder, this).import(node, sortNumber);
            }
            // if (nodeList.length) ui.notifications.warn(`Done importing ${nodeList.length + 1} encounter`);
        }

        async function processScenes() {
            console.log('processScenes START');
            let folder = await Utils.createFolder(`${this.importData.importName}-scene`, 'Scene');

            let nodeList = xpath.getNodeList(data, `/root/image/*`);
            nodeList.sort(Utils.sortNodeByName);
            let sortNumber = 0;
            for (const node of nodeList) {
                sortNumber += 1;
                await new Scenes(node, folder, this, sortNumber).importScene();
            }
            // if (nodeList.length) ui.notifications.warn(`Done importing ${nodeList.length + 1} scenes`);
        }

        /**
         *
         * Scan descriptions/text fields for links to previous data and map to new data.
         *
         * @param {*} importData
         */
        function convertLinksInDescriptions(exportPack) {
            console.log('xml-import.js convertLinksInDescriptions starting');
            // actors
            if (this.importData?.actorImported)
                for (const actorRec of this.importData.actorImported) {
                    // game.packs.get("world.Build_Evil_Tide-npc").get("IdEynqr8RgQFl2sO")
                    const actor = exportPack ? game.packs.get(actorRec.pack).get(actorRec.id) : game.actors.get(actorRec.id);
                    if (actor)
                        actor.update({
                            'system.details.biography.value': Utils.reformatMarkup(
                                actor.system.details.biography.value,
                                this,
                                600
                            ),
                        });
                }
            //items
            if (this.importData?.itemImported)
                for (const itemRec of this.importData.itemImported) {
                    // const item = game.packs.get(itemRec.pack).get(itemRec.id);
                    // console.log("xml-import.js convertLinksInDescriptions", { itemRec });
                    const item = exportPack ? game.packs.get(itemRec.pack).get(itemRec.id) : game.items.get(itemRec.id);
                    if (item) {
                        item.update({
                            'system.description': Utils.reformatMarkup(item.system.description, this, 600),
                        });
                        item.update({
                            'system.dmonlytext': Utils.reformatMarkup(item.system.dmonlytext, this, 600),
                        });
                    }
                }
            if (this.importData?.tableImported)
                for (const tableRec of this.importData.tableImported) {
                    // const table = game.packs.get(tableRec.pack).get(tableRec.id);
                    // const table = exportPack ? game.packs.get(tableRec.pack).get(tableRec.id) : game.tables.get(tableRec.id);
                    const table = game.tables.get(tableRec.id);
                    // console.log("xml-import.js convertLinksInDescriptions starting Tables===>", { table });
                    if (table)
                        table.update({
                            description: Utils.reformatMarkup(table.description, this, 600),
                        });
                }
            // journals
            if (this.importData?.imported?.['journalentry'])
                for (const journalRec of this.importData.imported['journalentry']) {
                    const journal = game.journal.get(journalRec.id);
                    if (journal)
                        journal.update({
                            content: Utils.reformatMarkup(journal.content, this, 600),
                        });
                }
        }

        ui.notifications.info(
            `Import of ${this.importData.importName} STARTING - Do not shutdown Foundry or reload while running.`,
            { permanent: true }
        );

        // If the data files are large this is required
        // const EXPORT_TYPE = 'compendium';
        const EXPORT_TYPE = 'world';
        //

        // import scenes
        await processScenes.bind(this)();
        // // import npcs
        await processNPCs.bind(this)(EXPORT_TYPE);
        // get everything in item/*
        await processItems.bind(this)(EXPORT_TYPE, '/root/item');
        // get spells (they are items in Foundry)
        await processItems.bind(this)(EXPORT_TYPE, '/root/spell', 'spell');
        // skill needs to be before race/background/class
        await processItems.bind(this)(EXPORT_TYPE, '/root/skill', 'skill');
        await processItems.bind(this)(EXPORT_TYPE, '/root/race', 'race');
        await processItems.bind(this)(EXPORT_TYPE, '/root/background', 'background');
        await processItems.bind(this)(EXPORT_TYPE, '/root/class', 'class');
        // get all story/encounters and place into journal
        await processBundles.bind(this)(EXPORT_TYPE);
        await processEncounters.bind(this)('battle');
        await processEncounters.bind(this)('battlerandom');
        await processTables.bind(this)(EXPORT_TYPE);
        await processJournals.bind(this)();

        this.maps.journalManuals = await new JournalManuals(this.importData.folder, this).import();

        convertLinksInDescriptions.bind(this)(EXPORT_TYPE === 'compendium', this.importData);

        console.log('Finished import, re-scanning text fields for links to imported data...');
        for (const [key, item] of this.maps.items) {
            const newText = Utils.finalMarkups(item.system?.description, this);
            const newGMText = Utils.finalMarkups(item.system?.dmonlytext, this);
            item.update({ 'system.description': newText }, { 'system.dmonlytext': newGMText });
        }
        for (const [key, actor] of this.maps.npcs) {
            const newText = Utils.finalMarkups(actor.system?.details?.biography?.value, this);
            actor.update({ 'system.details.biography.value': newText });
        }
        for (const [key, table] of this.maps.tables) {
            const newText = Utils.finalMarkups(table?.description, this);
            table.update({ description: newText });
        }

        // dont use this, do it during journal creation
        // for (const [key, journal] of this.maps.journalManuals) {
        //     for (const page of journal?.collections?.pages) {
        //         console.log(`${page.name}`, { page }, duplicate(page.text.content));
        //         page.update({ 'text.content': Utils.reformatMarkup(page.text.content, this.context, 200) });
        //     }
        // }

        console.warn('xml-import.js DONE=>', this);
        ui.notifications.info(`Import of ${this.importData.importName} COMPLETED.`, {
            permanent: true,
        });

        // game.ars.initLibrary.default();
    } // end parseXML
}
export default XmlImporter;
