import { Utils } from './utilities.js';
import * as xpath from './xpath.js';

export class Effects {
    /**
     *
     * Best effort convert effects to changes[].
     *
     * @param {FG effect label} effectLabel
     * @returns
     */
    static _effect(effectLabel, data) {
        effectLabel = effectLabel.replace(/\$ARCANE/gi, '@rank.levels.arcane');
        effectLabel = effectLabel.replace(/\$DIVINE/gi, '@rank.levels.divine');
        effectLabel = effectLabel.replace(/\$STR/gi, `@abilities.str.value`);
        effectLabel = effectLabel.replace(/\$DEX/gi, `@abilities.dex.value`);
        effectLabel = effectLabel.replace(/\$CON/gi, `@abilities.con.value`);
        effectLabel = effectLabel.replace(/\$WIS/gi, `@abilities.wis.value`);
        effectLabel = effectLabel.replace(/\$INT/gi, `@abilities.int.value`);
        effectLabel = effectLabel.replace(/\$CHA/gi, `@abilities.cha.value`);

        let skipAdd = false;
        let inlineAdd = false;
        let changes = [];
        const _MODES = game.ars.const.ACTIVE_EFFECT_MODES;
        const effects = effectLabel.split(';');
        // console.log('effectsMAnager.js _effect effects', effects);
        effects.forEach((element) => {
            if (element.length) {
                let effectRaw = element.trim();
                let key = effectRaw;
                let value = 'SPELL';
                let mode = _MODES.CUSTOM;
                const effectList = effectRaw.match(/^([A-Z]+):(.*)$/);
                if (effectList) {
                    const effect = effectList[1].trim();
                    let effectRemainer = effectList[2] ? effectList[2].trim() : '';
                    // in foundry we use [[formula]] and not [formula]
                    effectRemainer = effectRemainer.replace(/\[/, '[[');
                    effectRemainer = effectRemainer.replace(/\]/, ']]');
                    switch (effect) {
                        case 'ATK':
                            key = 'system.mods.attack.value';
                            mode = _MODES.ADD;
                            value = effectRemainer;
                            break;
                        case 'DMG':
                            key = 'system.mods.damage.value';
                            mode = _MODES.ADD;
                            value = effectRemainer;
                            break;

                        case 'PARALYZATION':
                        case 'POISON':
                        case 'DEATH':
                        case 'ROD':
                        case 'STAFF':
                        case 'WAND':
                        case 'PETRIFICATION':
                        case 'POLYMORPH':
                        case 'BREATH':
                        case 'SPELL':
                            // key = `data.saves.${effect.toLowerCase()}`;
                            key = `system.mods.saves.${effect.toLowerCase()}`;
                            if (effectRemainer.includes(',')) {
                                effectRemainer = effectRemainer.replace(/,/, ' ');
                                key = `system.mods.saves.${effect.toLowerCase()}`;
                            }
                            mode = _MODES.CUSTOM;
                            value = `"formula": "${effectRemainer}", "properties":""`;
                            break;

                        case 'SAVE':
                            if (effectRemainer.includes(',')) {
                                effectRemainer = effectRemainer.replace(/,/, ' ');
                            }
                            key = 'system.mods.saves.all';
                            mode = _MODES.ADD;
                            value = `"formula": "${effectRemainer}", "properties":""`;
                            break;

                        case 'CHECK':
                            key = 'system.mods.checks.all';
                            mode = _MODES.ADD;
                            value = effectRemainer;
                            break;
                        case 'MELEEAC':
                        case 'AC':
                            key = 'system.mods.ac.value';
                            mode = _MODES.ADD;
                            value = effectRemainer;
                            break;
                        // case 'MELEEAC':
                        //     key = "system.mods.ac.melee.value";
                        //     mode = _MODES.ADD;
                        //     value = effectRemainer;
                        //     break;
                        case 'RANGEAC':
                            key = 'system.mods.ac.ranged.value';
                            mode = _MODES.ADD;
                            value = effectRemainer;
                            break;

                        case 'BAC':
                            key = 'system.mods.ac.base';
                            mode = _MODES.DOWNGRADE;
                            value = effectRemainer;
                            break;
                        case 'BRANGEAC':
                            key = 'system.mods.ac.ranged.base';
                            mode = _MODES.DOWNGRADE;
                            value = effectRemainer;
                            break;
                        // case 'BMELEEAC':
                        //     key = "system.mods.ac.melee.value";
                        //     mode = _MODES.DOWNGRADE;
                        //     value = effectRemainer;
                        //     break;
                        case 'BAC': // ARMORBASE
                        case 'BMELEEAC':
                            key = 'system.mods.ac.base';
                            mode = _MODES.DOWNGRADE;
                            value = effectRemainer;
                            break;

                        case 'DDR': // DDR: -1 fire
                            if (effectRemainer) {
                                // console.log('effectsManager.js DDR: effectRemainer', effectRemainer);
                                // let [match, newValue, newKey] = effectRemainer.match(/^(\d+) (\w+)/);
                                const match = effectRemainer.match(/^(\d+) (\w+)/);
                                // console.log("effectsManager.js DDR:", { match, newKey, newValue });
                                // if (newKey && newValue) {
                                if (match && match.length == 3 && match[2] && match[1]) {
                                    key = `system.mods.resists.perdice.${match[2].trim()}`;
                                    mode = _MODES.UPGRADE;
                                    value = match[1].trim();
                                }
                            }
                            break;

                        case 'RESIST':
                            //TODO find way to parse this into magic potency
                            if (effectRemainer.includes('!')) {
                                skipAdd = true;
                                inlineAdd = true;
                            } else {
                                mode = _MODES.CUSTOM;
                                key = 'system.mods.resist';
                                value = effectRemainer;
                            }
                            break;
                        case 'IMMUNE':
                            if (effectRemainer.includes('!')) {
                                skipAdd = true;
                            } else {
                                mode = _MODES.CUSTOM;
                                key = 'system.mods.immune';
                                value = effectRemainer;
                            }
                            break;
                        case 'VULN':
                            if (effectRemainer.includes('!')) {
                                skipAdd = true;
                            } else {
                                mode = _MODES.CUSTOM;
                                key = 'system.mods.vuln';
                                value = effectRemainer;
                            }
                            break;

                        case 'STR':
                            key = 'system.abilities.str.value';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'DEX':
                            key = 'system.abilities.dex.value';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'CON':
                            key = 'system.abilities.con.value';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'CHA':
                            key = 'system.abilities.cha.value';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'INT':
                            key = 'system.abilities.int.value';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'WIS':
                            key = 'system.abilities.wis.value';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;

                        ///
                        case 'BSTR':
                            key = 'system.abilities.str.value';
                            value = effectRemainer;
                            mode = _MODES.OVERRIDE;
                            break;
                        case 'BDEX':
                            key = 'system.abilities.dex.value';
                            value = effectRemainer;
                            mode = _MODES.OVERRIDE;
                            break;
                        case 'BCON':
                            key = 'system.abilities.con.value';
                            value = effectRemainer;
                            mode = _MODES.OVERRIDE;
                            break;
                        case 'BCHA':
                            key = 'system.abilities.cha.value';
                            value = effectRemainer;
                            mode = _MODES.OVERRIDE;
                            break;
                        case 'BINT':
                            key = 'system.abilities.int.value';
                            value = effectRemainer;
                            mode = _MODES.OVERRIDE;
                            break;
                        case 'BWIS':
                            key = 'system.abilities.wis.value';
                            value = effectRemainer;
                            mode = _MODES.OVERRIDE;
                            break;
                        //
                        case 'BPSTR':
                            key = 'system.abilities.str.percent';
                            value = effectRemainer;
                            mode = _MODES.OVERRIDE;
                            break;
                        case 'BPDEX':
                            key = 'system.abilities.dex.percent';
                            value = effectRemainer;
                            mode = _MODES.OVERRIDE;
                            break;
                        case 'BPCON':
                            key = 'system.abilities.con.percent';
                            value = effectRemainer;
                            mode = _MODES.OVERRIDE;
                            break;
                        case 'BPCHA':
                            key = 'system.abilities.cha.percent';
                            value = effectRemainer;
                            mode = _MODES.OVERRIDE;
                            break;
                        case 'BPINT':
                            key = 'system.abilities.int.percent';
                            value = effectRemainer;
                            mode = _MODES.OVERRIDE;
                            break;
                        case 'BPWIS':
                            key = 'system.abilities.wis.percent';
                            value = effectRemainer;
                            mode = _MODES.OVERRIDE;
                            break;
                        //
                        case 'PSTR':
                            key = 'system.abilities.str.percent';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'PDEX':
                            key = 'system.abilities.dex.percent';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'PCON':
                            key = 'system.abilities.con.percent';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'PCHA':
                            key = 'system.abilities.cha.percent';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'PINT':
                            key = 'system.abilities.int.percent';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'PWIS':
                            key = 'system.abilities.wis.percent';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;

                        case 'INIT':
                            key = 'system.mods.init.value';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;

                        case 'HEAL':
                            key = 'system.mods.heal.value';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;

                        case 'REGEN':
                            key = 'special.ongoing';
                            value = `{"type":"heal", "rate":"1", "cycle": "round", "dmgType": "","formula":"${effectRemainer}"}`;
                            mode = _MODES.CUSTOM;
                            break;

                        case 'HEALX':
                            key = `system.mods.heal.multiplier`;
                            value = effectRemainer;
                            mode = _MODES.UPGRADE;
                            break;

                        case 'MOVE':
                            key = 'system.attributes.movement.value';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'BASEMOVE':
                            key = 'system.attributes.movement.value';
                            value = effectRemainer;
                            mode = _MODES.UPGRADE;
                            break;
                        case 'SURPRISE':
                            key = 'system.mods.surprise.value';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'HP':
                            key = 'system.attributes.hp.base';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'TURN':
                            key = 'system.mods.turn.value';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'TURNRANK':
                            key = 'system.mods.turn.rank';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'ARCANE':
                            key = 'system.mods.levels.arcane';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;
                        case 'DIVINE':
                            key = 'system.mods.levels.divine';
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;

                        case 'DMGX':
                            key = `system.mods.damage.multiplier`;
                            value = effectRemainer;
                            mode = _MODES.UPGRADE;
                            break;

                        case 'DMGO':
                            {
                                let formula, damageType;
                                [formula, damageType] = effectRemainer.split(' ');
                                // console.log("effetsManager.js DMGO", { effectRemainer, formula, damageType })
                                // key = `dmg.ongoing ${formula}`;
                                key = `special.ongoing`;
                                try {
                                    // value = formula + ' ' + damageType.split(',', 1);
                                    value = `"type":"damage", "rate":"1", "cycle": "round", "dmgType": "${
                                        damageType.split(',')[0] || ''
                                    }","formula":"${formula}"`;
                                } catch (err) {
                                    value = `"type":"damage", "rate":"1", "cycle": "round", "dmgType": ""}","formula":"${formula}"`;
                                }
                                mode = _MODES.CUSTOM;
                            }
                            break;

                        case 'DA': // DA: ## type, DA: 20 fire
                            key = `special.absorb`;
                            mode = _MODES.CUSTOM;
                            value = `{"amount":${effectRemainer.split(' ')[0]}, "damageType":"${
                                effectRemainer.split(' ')[1]
                            }"}`;
                            break;

                        case 'MIRRORIMAGE':
                            key = `special.mirrorimage`;
                            value = effectRemainer;
                            mode = _MODES.CUSTOM;
                            break;
                        case 'STONESKIN':
                            key = `special.stoneskin`;
                            value = effectRemainer;
                            mode = _MODES.CUSTOM;
                            break;

                        case 'MR':
                            key = `system.mods.magic.resist`;
                            value = effectRemainer;
                            mode = _MODES.ADD;
                            break;

                        case 'IF':
                            key = `attacker.XXXXX`;
                            // value = effectRemainer;
                            value = '{"trigger": "XXX", "properties": "", "type": "attack", "formula": "1"}';
                            mode = _MODES.CUSTOM;
                            break;

                        case 'IFT':
                            // Regex to match and extract the type and values
                            const typeAndValuesRegex = /TYPE\((.*?)\)/;
                            const typeAndValueMatch = effectRemainer.match(typeAndValuesRegex);
                            // values (orc,ogre)
                            const typeValues = typeAndValueMatch?.[1] ?? 'UNKNOWN';

                            // Regex to match and extract the mode (ATK, DMG, or AC) and its value
                            const modeRegx = /(ATK|DMG|AC):([^\s;]+)/;
                            const modeMatch = effectRemainer.match(modeRegx);
                            // Store the mode (ATK, DMG, or AC)
                            const actionMode = modeMatch?.[1] ?? 'NOMATCH';
                            // Store the value associated with the mode
                            const modeValue = parseInt(modeMatch?.[2] ?? 1, 10);
                            let triggerAction = 'attack';
                            switch (actionMode.toLowerCase().trim()) {
                                case 'atk':
                                    {
                                        triggerAction = 'attack';
                                    }
                                    break;

                                case 'dmg':
                                    {
                                        triggerAction = 'damage';
                                    }
                                    break;
                                default:
                                    {
                                        triggerAction = 'NOMATCH';
                                    }
                                    break;
                            }
                            key = `target.type`;
                            // value = effectRemainer;
                            value = `{"trigger": "${typeValues}", "properties": "", "type": "${triggerAction}", "formula": "${modeValue}"}`;
                            mode = _MODES.CUSTOM;
                            break;

                        default:
                    }
                }
                if (!inlineAdd && !skipAdd) {
                    changes.push({
                        key: key,
                        value: value,
                        mode: mode,
                    });
                }
            }
        });
        return changes;
    }

    static getEffects(node, nameEffect = '') {
        let effects = [];
        for (const effectNode of xpath.queryXPathAll(node, `effectlist/*`)) {
            let old = {
                name: xpath.getNodeValue(effectNode, 'name'),
                label: xpath.getNodeValue(effectNode, 'effect'),
                onItemUseOnly: xpath.getNodeValue(effectNode, 'actiononly', '1') === '1',
            };

            const changes = Effects._effect(old.label);
            let effect = {
                _id: foundry.utils.randomID(16),
                label: nameEffect ? nameEffect : old.name,
                disabled: false,
                transfer: !old.onItemUseOnly,
                changes: changes,
            };
            effects.push(effect);
        }

        return effects;
    }
} // end class Effects
