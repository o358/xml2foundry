import * as xpath from './xpath.js';

export class Utils {
    /**
     *
     * Remove the 000.000 from string (names)
     *
     * @param {*} rawName
     * @returns
     */
    static cleanSortingName(rawName) {
        let newName = rawName;
        const match = rawName.match(/(^[\d\.]+\s+)?(.*)$/i);
        // console.log("Utils.js cleanSortingName", { match });
        if (match && match[1] && match[2]) {
            newName = match[2].trim();
        }
        newName = newName.replace(/^_/, '');
        newName = newName.replace(/\\n/g, '');
        return newName;
    }

    static getNodeById(id, nodeList) {
        let node = undefined;

        // console.log("Utils.js getNodeById id", { id });
        for (const entry of nodeList) {
            if (id === entry.nodeName) {
                node = entry;
                break;
            }
        }

        // console.log("Utils.js getNodeById node", node);
        return node;
    }
    static getPackByNodeId(collection, id, typeImported) {
        let pack = collection;
        if (!typeImported) return collection;
        for (const entry of typeImported) {
            if (id.nodeName === entry.nodeId) {
                pack = entry.pack;
                break;
            }
        }
        return pack;
    }
    static replaceTag(text, tag, replacement) {
        let output = text;
        if (output) {
            try {
                output = output.replace(new RegExp(`<${tag}>`, 'g'), `<${replacement}>`);
                output = output.replace(new RegExp(`</${tag}>`, 'g'), `</${replacement}>`);
            } catch (err) {
                ui.notifications.error(`error in replaceTag: ${err}`);
                console.log(`error in replaceTag: ${err}`, { text, tag, replacement });
            }
        }
        return output;
    }

    // static reformatMarkup(text, importData, imgsize = '200') {
    //     console.log("utilities.js reformatMarkup", text)

    //     /**
    //      * for some reason imagewindow get ripped out once it's in a journal
    //      * so we need to process the image links before we create the journal otherwise
    //      * will not exist
    //      */

    //     // console.log("utilities.js reformatMarkup", text)
    //     function getImageLink(match, p1, p2, offset, string) {
    //         let path = '';
    //         // console.log("Utils.js getImagePath", { match, p1, p2, offset, string });
    //         let imgPath = undefined;
    //         let m = p1.match(/image\.(id-[0-9]+)(@.*)?$/)
    //         // console.log("Utils.js getImagePath m", m, m[0], m[1]);
    //         let id = m[1];
    //         // console.log("Utils.js getImagePath id", id);
    //         const imgNode = getNodeById(id, importData.imgNodeList);
    //         // console.log("Utils.js getImagePath imgNode", { imgNode });
    //         if (imgNode) {
    //             imgPath = xpath.getNodeValue(imgNode, `image/layers/layer/bitmap`);
    //             if (!imgPath) imgPath = xpath.getNodeValue(imgNode, `image/bitmap`); // FGC?
    //             // console.log("Utils.js getImagePath imgPath 1", { imgPath });
    //             if (imgPath) {
    //                 imgPath = imgPath.replace(/(@.*)?$/, '');
    //                 // strip campaign/ out completely
    //                 imgPath = imgPath.replace("campaign/", '');
    //                 // console.log("Utils.js getImagePath imgPath 2", { imgPath });
    //                 if (imgPath) {
    //                     path = `<img src="${importData.importPath}/${importData.importName}/${imgPath}" alt="${p2}" height="${imgsize}" />`
    //                 }
    //             }
    //         }
    //         // console.log("Utils.js getImagePath path", path);
    //         return path;
    //     }

    //     //<link class="imagewindow" recordname="image.id-00024">Image: Selfie With Statues</link>
    //     text = text.replace(new RegExp(/<link class="imagewindow" recordname="(.*)">(.*)<\/link>/, "ig"), getImageLink);

    //     return text;
    // }

    static stripAfterAt(inputString) {
        // Check if inputString is a valid string
        if (typeof inputString !== 'string') {
            return undefined;
        }

        // Find the index of the '@' symbol
        const atSymbolIndex = inputString.indexOf('@');

        // If '@' is present, return the substring before it
        // Otherwise, return the original string
        return atSymbolIndex !== -1 ? inputString.substring(0, atSymbolIndex) : inputString;
    }

    static reformatMarkup(text, context, imgsize = '200') {
        if (!text) return '';

        // console.log("utilities.js reformatMarkup", text)
        function getImageLink(match, p1, p2, offset, string) {
            let path = '';
            // console.log("Utils.js getImagePath", { match, p1, p2, offset, string });
            let imgPath = undefined;
            let m = p1.match(/image\.id-([0-9]+)(@.*)?$/);
            // console.log("Utils.js getImagePath m", m, m[0], m[1]);
            let id = m?.[1];
            if (id) {
                // console.log("Utils.js getImagePath id", id);
                const imgNode = Utils.getNodeById(id, context.importData.imgNodeList);
                // console.log("Utils.js getImagePath imgNode", { imgNode });
                if (imgNode) {
                    imgPath = xpath.getNodeValue(imgNode, `image/layers/layer/bitmap`);
                    if (!imgPath) imgPath = xpath.getNodeValue(imgNode, `image/bitmap`); // FGC?
                    // console.log("Utils.js getImagePath imgPath 1", { imgPath });
                    if (imgPath) {
                        imgPath = imgPath.replace(/(@.*)?$/, '');
                        // strip campaign/ out completely
                        imgPath = imgPath.replace('campaign/', '');
                        // console.log("Utils.js getImagePath imgPath 2", { imgPath });
                        if (imgPath) {
                            // path = `<img src="${context.importData.importPath}/${context.importData.importName}/${imgPath}" alt="${p2}" height="${imgsize}" />`;
                            // removed size piece, foundry scales to window size
                            path = `<img src="${context.importData.importPath}/${context.importData.importName}/${imgPath}" alt="${p2}"/>`;
                        }
                    }
                }
                // console.log("Utils.js getImagePath path", path);
            }
            return path;
        }

        // replace dice roll like text with an actual clickable roll!
        function makeInlineDiceRolls(match, p1, p2, offset, string) {
            // console.log("Utils.js makeInlineDiceRolls", { match, p1, p2, offset, string });
            let formula = `${p1}`;
            if (p1) {
                formula = `[[/roll ${p1}]]`;
            }
            return formula;
        }

        function getLinkURL(type, recordName, linkText) {
            recordName = Utils.stripAfterAt(recordName);
            // Create a dynamic regular expression using the 'type' variable
            const regex = new RegExp('(id-\\d+)');

            // Use the dynamic regex to execute the match
            const idMatch = regex.exec(recordName);

            const id = idMatch ? idMatch[1] : null;
            let path = `@UUID[${id ? id : recordName}]\{${linkText}\}<p />`;

            if (id) {
                const entry = context?.maps?.[`${type}`]?.get(id);
                if (entry) path = `@UUID[${entry?.uuid}]\{${linkText}\}<p />`;
                else path = `@UUID[${type.toUpperCase()}ID=${id}]\{${linkText}\}<p />`;
            } else {
                let a = null;
            }

            return path;
        }

        // text = text.replace(/<link class="(\w+)" recordname="(.*?)">(.*?)<\/link>/gi, getLinkURL);
        /**
         *
         * Build a link to insert into journal
         *
         * @param {*} type actor or item
         * @param {*} linkText default link text
         * @param {*} text  the text that will show if the link is valid
         * @param {*} record id-00000
         * @param {*} importList importData.imported['encounter']
         * @returns
         */
        // function getLinkText(type, linkText, text, record, importList) {
        //     //@Item[QITCR2Z7CgxqSbXc]{TEST item}
        //     //@Actor[5LfBrdoCiY7vKylV]{TestNPC}
        //     //@Compendium[world.npc.SeRkkLaomKRxtjQc]{TestNPC}
        //     const r = Utils.getImportedRecord(record, importList);
        //     if (r) {
        //         if (type === 'actor') {
        //             // console.log("Utils.js getLinkText actor", { r });
        //             if (r.pack) {
        //                 linkText = `@Compendium[${r.pack}.${r.id}]\{${text}\}<p />`;
        //             } else {
        //                 linkText = `@Actor[${r.id}]\{${text}\}<p />`;
        //             }
        //         } else if (type === 'rolltable') {
        //             // assume item
        //             // console.log("Utils.js getLinkText item", { r });
        //             if (r.pack) {
        //                 linkText = `@Compendium[${r.pack}.${r.id}]\{${text}\}<p />`;
        //             } else {
        //                 linkText = `@RollTable[${r.id}]\{${text}\}<p />`;
        //             }
        //         } else if (type === 'journalentry') {
        //             // assume item
        //             // console.log("Utils.js getLinkText item", { r });
        //             if (r.pack) {
        //                 linkText = `@Compendium[${r.pack}.${r.id}]\{${text}\}<p />`;
        //             } else {
        //                 linkText = `@JournalEntry[${r.id}]\{${text}\}<p />`;
        //             }
        //         } else {
        //             // assume item
        //             // console.log("Utils.js getLinkText item", { r });
        //             if (r.pack) {
        //                 linkText = `@Compendium[${r.pack}.${r.id}]\{${text}\}<p />`;
        //             } else {
        //                 linkText = `@Item[${r.id}]\{${text}\}<p />`;
        //             }
        //         }
        //     }
        //     return linkText;
        // }

        // this.maps = {
        //     scenes: new Map(),
        //     npcs: new Map(),
        //     journals: new Map(),
        //     items: new Map(),
        //     tables: new Map(),
        //     encounters: new Map(),
        // };
        function getBattleLink(match, p1, p2, offset, string) {
            return getLinkURL('encounters', p1, p2);
        }
        function getBundleLink(match, p1, p2, offset, string) {
            return getLinkURL('bundles', p1, p2);
        }
        function getNPCLink(match, p1, p2, offset, string) {
            return getLinkURL('npcs', p1, p2);
        }

        function getTableLink(match, p1, p2, offset, string) {
            return getLinkURL('tables', p1, p2);
        }

        function getJournalLink(match, p1, p2, offset, string) {
            return getLinkURL('journals', p1, p2);
        }

        function getItemLink(match, p1, p2, offset, string) {
            return getLinkURL('items', p1, p2);
        }

        try {
            text = Utils.replaceTag(text, 'h', 'h1');
            text = Utils.replaceTag(text, 'frame', 'blockquote');
            // make inline dice rollable
            text = text.replace(new RegExp(/(\d+d\d+([\s]?[\+\-][\s]?\d+)?)/, 'ig'), makeInlineDiceRolls);
            text = text.replace(new RegExp(/\s(d\d+)/, 'ig'), ' [[/roll 1$1]]');

            // //<link class="imagewindow" recordname="image.id-00024">Image: Selfie With Statues</link>
            text = text.replace(new RegExp(/<link class="imagewindow" recordname="(.*?)">(.*?)<\/link>/, 'ig'), getImageLink);

            //<link class="item" recordname="item.id-00002">Item: Banded Mail</link>
            text = text.replace(new RegExp(/<link class="item" recordname="(.*?)">(.*?)<\/link>/, 'ig'), getItemLink);

            // //<link class="battle" recordname="battle.id-00012">Encounter: Bee x5</link>
            text = text.replace(new RegExp(/<link class="battle" recordname="(.*?)">(.*?)<\/link>/, 'ig'), getBattleLink);

            // //<link class="treasureparcel" recordname="treasureparcels.id-00004">Parcel: TREASURE ROOM</link>
            text = text.replace(
                new RegExp(/<link class="treasureparcel" recordname="(.*?)">(.*?)<\/link>/, 'ig'),
                getBundleLink
            );

            // // <link class="npc" recordname="npc.id-00012">NPC: bugmen</link>
            text = text.replace(new RegExp(/<link class="npc" recordname="(.*?)">(.*?)<\/link>/, 'ig'), getNPCLink);

            // <link class="table" recordname="tables.id-00018">Table: Confusion</link>
            text = text.replace(new RegExp(/<link class="table" recordname="(.*?)">(.*?)<\/link>/, 'ig'), getTableLink);

            // // <link class="encounter" recordname="reference.hiddenstory.id-00923@AD&amp;D 2E Players Handbook">Table 9: Constitution Saving Throw Bonuses</link>
            text = text.replace(new RegExp(/<link class="encounter" recordname="(.*?)">(.*?)<\/link>/, 'ig'), getJournalLink);

            // getLinkURL
            // text = text.replace(new RegExp(/<link class="(\w+)" recordname="(.*?)">(.*?)<\/link>/, 'ig'), getLinkURL);

            // cleanup "chat" style frames
            text = text.replace(new RegExp(/<frameid>(.*?)<\/frameid>/, 'ig'), 'Speaker: $1');
            // remove \n in the document
            text = text.replace(new RegExp(/\\n/, 'g'), '');
            // remove linklist text
            //<linklist></linklist>
            text = text.replace(new RegExp(/<\/?linklist>/, 'g'), '');
            //<list></list> replace with <ul></ul>
            text = text.replace(new RegExp(/<(\/)?list>/, 'g'), '<$1ul>');

            // can't reall get these links since... uh, we're making the journals last so we can find all the others
            //<link class="encounter" recordname="encounter.id-00108">Story: The Revolving Passage</link>
        } catch (err) {}

        return text;
    }

    static finalMarkups(text, context) {
        if (text) {
            // text = text.replace(/IMAGEID=(\d+)/g, (match, id) => context.maps.images.get(id));
            text = text.replace(/ACTORID=(\d+)/g, (match, id) => context.maps.npcs.get(id)?.uuid);
            text = text.replace(/ITEMID=(\d+)/g, (match, id) => context.maps.items.get(id)?.uuid);
            text = text.replace(/TABLEID=(\d+)/g, (match, id) => context.maps.tables.get(id)?.uuid);
        }

        return text;
    }

    /**
     *
     *
     *
     * @param {node} a
     * @param {node} b
     * @returns
     */
    static sortNodeByName(a, b) {
        const nameA = xpath.getNodeValue(a, 'name').toUpperCase(); // ignore upper and lowercase
        const nameB = xpath.getNodeValue(b, 'name').toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }

        // names must be equal
        return 0;
    }

    /**
     *
     * Sort node by "level"
     *
     * @param {*} a
     * @param {*} b
     * @returns
     */
    static sortNodeByLevel(a, b) {
        const levelA = parseInt(xpath.getNodeValue(a, 'level', '0'));
        const levelB = parseInt(xpath.getNodeValue(b, 'level', '0'));
        if (levelA < levelB) {
            return -1;
        }
        if (levelA > levelB) {
            return 1;
        }
        return 0;
    }

    /**
     *
     * sort node by 'order'
     *
     * @param {*} a
     * @param {*} b
     * @returns
     */
    static sortNodeByOrder(a, b) {
        const levelA = parseInt(xpath.getNodeValue(a, 'order', '0'));
        const levelB = parseInt(xpath.getNodeValue(b, 'order', '0'));
        if (levelA < levelB) {
            return -1;
        }
        if (levelA > levelB) {
            return 1;
        }
        return 0;
    }

    /**
     *
     * Sort node by it's id-###### numbers
     *
     * @param {*} array
     * @returns
     */
    static sortById(array) {
        if (Array.isArray(array)) {
            return array.sort((a, b) => {
                // Extract the numeric part of the id and convert to integer
                const idA = parseInt(a.id.split('-')[1], 10);
                const idB = parseInt(b.id.split('-')[1], 10);

                // Compare the integer values to determine the order
                return idA - idB;
            });
        } else {
            console.error('sortById is not array ', { array });
        }
        return 0;
    }

    // calculate formula for caster levels
    static _fgDiceConversion(dice) {
        // console.log("utilitis.js _fgDiceConversion", { dice })
        let dicefinal = dice;
        if (dice) {
            // look for d6,d6,d6,d3 type dice entries
            if (dice.match(/(,d\d+)/gi)) {
                const diceMerged = dice.split(',');
                let diceFormula = '';
                // map creates {"d6":"3", "d4":"2" ,...}
                let diceMapCount = diceMerged.reduce((cnt, cur) => ((cnt[cur] = cnt[cur] + 1 || 1), cnt), {});
                // console.log("utilitis.js _fgDiceConversion", { diceMapCount })
                Object.keys(diceMapCount).forEach((die, index) => {
                    // flip through map of matches/counts
                    diceFormula = diceFormula + (diceFormula.length ? '+' : '') + `${diceMapCount[die]}${die}`;
                });
                dicefinal = diceFormula;
            }
        }
        // console.log("utilitis.js _fgDiceConversion", { dicefinal })
        return dicefinal;
    }

    static getItemType(nodeType, nodeSubType) {
        let itemType = 'item';
        if (['weapon', 'staff', 'rod'].includes(nodeType) || ['weapon', 'staff', 'rod'].includes(nodeSubType)) {
            itemType = 'weapon';
        } else if (
            ['armor', 'warding', 'cloak', 'ring', 'shield'].includes(nodeType) ||
            ['armor', 'warding', 'cloak', 'ring', 'shield'].includes(nodeSubType)
        ) {
            itemType = 'armor';
        } else if (['potion'].includes(nodeType) || ['potion'].includes(nodeSubType)) {
            itemType = 'potion';
        }

        return itemType;
    }

    /**
     *
     * Get Compendium data
     *
     * @param {*} compendiumName
     * @param {*} myType
     * @returns
     */
    static async getComp(compendiumName, myType) {
        let pack = game.packs.find((p) => p.metadata.name === compendiumName);
        // console.log("utilties.js getComp pack", { pack });
        if (!pack) {
            pack = await CompendiumCollection.createCompendium({
                name: compendiumName,
                label: compendiumName,
                collection: compendiumName,
                type: myType,
            });
        }
        return pack;
    }

    /**
     *
     * Find imported record to map to it.
     *
     * @param {*} fgIdname 'id-00000'
     * @param {*} importList importData.actorImported
     * @returns record of data {r.id, r.img, r.name, r.type, r.pack}
     */
    static getImportedRecord(fgIdname, importList) {
        // console.log("Utils.js getImportedRecord", { fgIdname, importList });
        let record;
        if (importList) {
            for (const r of importList) {
                //{ nodeId: npcNode.nodeName, type: 'npc', name: actor.name, id: actor.id, img: actor.img }
                // console.log("Utils.js getImportedRecord", { r });
                if (r.nodeId === fgIdname) {
                    record = {
                        id: r.id,
                        type: r.type,
                        img: r.img,
                        name: r.name,
                        pack: r.pack ? r.pack : '',
                        folder: r.folder ? r.folder : '',
                    };
                }
            }
        }
        return record;
    }

    /**
     * Get the first id.00000 from a string of text
     *
     * @param {*} idtext
     * @returns
     */
    static getFGIdFromString(idtext) {
        let idname = idtext;
        const match = idtext.match(/\.?(id-\d+)/);
        if (match && match[1]) idname = match[1].trim();

        return idname;
    }

    static sleep(milliseconds) {
        return new Promise((resolve) => setTimeout(resolve, milliseconds));
    }

    /**
     *
     * @param {*} name
     * @param {*} type
     * @param {*} sortMethod
     * @returns
     */
    static async createFolder(name, type, sortMethod = 'a', forceNew = false) {
        const folderData = {
            name: name,
            type: type,
            sorting: sortMethod,
            sort: 0,
        };
        let folder = !forceNew ? game.folders.find((p) => p.name === name) : null;
        if (!folder) folder = await Folder.implementation.create(folderData);
        return folder;
    }

    /**
     *
     * Get or create folder if it doesn't exist
     *
     * @param {*} folderName
     * @param {*} folderData
     * @returns
     */
    static async getFolder(folderName, folderData) {
        let folder;
        if (folderData.parent) {
            folder = game.folders.find((p) => p.parentFolder?.id === folderData.parent && p.name === folderName);
        } else {
            folder = game.folders.find((p) => p.name === folderName);
        }

        if (!folder) folder = await Folder.implementation.create(folderData);
        return folder;
    }

    static _cleanDamage(dmg) {
        // Check if dmg is a non-empty string
        if (typeof dmg !== 'string' || !dmg.trim()) {
            return null;
        }

        dmg = dmg.trim();

        // Regular expression to match damage pattern including rolls like d6, d10+2, etc.
        const regex = /^(\d*[dD]\d+([\-\+]\d+)?)/;
        const match = dmg.match(regex);

        // Check if the damage string matches the expected pattern
        if (match && match[1]) {
            return match[1]; // Cleaned damage
        } else {
            // Log an error or handle the case where the pattern does not match
            console.error(`Invalid damage format: ${dmg}`);
            return null;
        }
    }

    // get the first "valid" damage type (FG sometimes had multiple)
    static _cleanDamageType(dmg) {
        let dmgType = 'slashing';
        const types = dmg.match(/([a-z]+,?\s?)+$/i);
        if (types && types.length) {
            const typeList = types[0].split(',');
            for (const entry of typeList) {
                // typeList.forEach(entry => {
                if (!entry.match(/^(magic|large)/i)) {
                    dmgType = entry;

                    //remap some values
                    switch (dmgType) {
                        case 'frost':
                            dmgType = 'cold';
                            break;

                        default:
                            break;
                    }
                    return dmgType;
                }
            }
        }
        return dmgType;
    }
} // end class utils
