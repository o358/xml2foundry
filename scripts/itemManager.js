import XmlImporter from './xml-import.js';
import * as xpath from './xpath.js';
import { Actions } from './actions.js';
import { Effects } from './effectsManager.js';
import { Utils } from './utilities.js';

export class Items {
    /**
     *
     * @param {*} node
     * @param {*} folder
     * @param {*} context
     * @param {*} sortNumber
     * @param {*} options
     */

    constructor(node, itemType, exportPack, pack, folder, context, options = {}) {
        this.context = context;
        this.importData = context.importData;
        this.node = node;
        this.folder = folder;
        this.itemType = itemType;
        this.exportPack = exportPack;
        this.pack = pack;
        this.options = options;
    }

    /**
     *
     * @param {*} nodeSkill
     * @param {*} level
     * @returns
     */
    async processAdvancementSkills(nodeSkill, level) {
        const _cleanName = (name) => {
            let newName = name.replace(/\[\d+\]/, '');
            return newName.trim();
        };

        const _makeSkill = async (node) => {
            const itemCreate = new Items(node, 'skill', this.exportPack, this.pack, this.folder, this.context);
            await itemCreate.import(node, 'skill');
        };

        const _findSkill = async (skillName, skillNode) => {
            let skill;
            let found = false;
            if (this.importData.imported['skill']) {
                for (const existingSkill of this.importData.imported['skill']) {
                    if (skillName.toLowerCase() === existingSkill.name.toLowerCase()) {
                        skill = {
                            name: skillName,
                            type: 'skill',
                            img: existingSkill.img,
                            id: existingSkill.id,
                            uuid: existingSkill.uuid,
                            level: level,
                            count: 1,
                        };
                        found = true;
                        break;
                    }
                }
            }
            if (!found) {
                await _makeSkill.bind(this)(skillNode);
                skill = await _findSkill.bind(this)(skillName);
            }
            return skill;
        };

        let skills = [];

        let skillsList = xpath.getNodeList(nodeSkill, 'skilllist/*');
        for (const skillNode of skillsList) {
            const skillName = _cleanName(xpath.getNodeValue(skillNode, 'name'));
            skills.push(await _findSkill(skillName, skillNode));
        }

        return skills;
    }

    /**
     * Process features in class/race/backgrounds
     *
     * @param {*} nodeAbl
     * @param {*} path
     * @returns
     */
    async processItemFeatures(nodeAbl, path = 'features') {
        let featureItemList = [];
        const className = xpath.getNodeValue(nodeAbl, 'name');
        let featuresList = xpath.getNodeList(nodeAbl, `${path}/*`);
        for (const featureNode of featuresList) {
            const level = parseInt(xpath.getNodeValue(featureNode, 'level', '0'));
            const ablName = xpath.getNodeValue(featureNode, 'name');
            let featureData = {
                // description: Utils.reformatMarkup(xpath.getNodeValue(featureNode, 'text'), importData, 600),
                description: xpath.getNodeValue(featureNode, 'text'),
            };
            const actions = featureData.actions;
            delete featureData.actions;

            let itemNew = await Item.create(
                {
                    name: `${className}:${ablName} at level ${level}`,
                    type: 'ability',
                    img: game.ars.config.icons.general.items['ability'],
                    // img: 'icons/skills/social/wave-halt-stop.webp',
                    folder: this.exportPack ? '' : this.folder.id,
                    system: {
                        ...featureData,
                    },
                },
                {
                    displaySheet: false,
                    pack: this.exportPack ? this.pack.collection : null,
                }
            );

            itemNew.actionGroups = Actions.makeActionsGroup(itemNew, actions);
            game.ars.ARSActionGroup.saveAll(itemNew);

            featureItemList.push({
                name: `${className}:${ablName} at level ${level}`,
                type: 'ability',
                img: itemNew.img,
                id: itemNew.id,
                uuid: itemNew.uuid,
                level: level,
                count: 1,
            });
        }

        return featureItemList;
    }

    /**
     *
     * Import procificies attached to a background/race/class?
     *
     * @param {*} node
     * @param {*} importData
     * @param {*} pack
     * @returns
     */
    async processItemProficiencies(node) {
        let createdProfs = [];
        const sourceName = xpath.getNodeValue(node, 'name');

        let profList = xpath.getNodeList(node, 'proficiencies/*');
        for (const nodeProf of profList) {
            const profName = xpath.getNodeValue(nodeProf, 'name');
            let data = {
                hit: xpath.getNodeValue(nodeProf, 'hitadj'),
                damage: xpath.getNodeValue(nodeProf, 'dmgadj'),
                // description: Utils.reformatMarkup(xpath.getNodeValue(nodeProf, 'text'), importData, 600),
                description: xpath.getNodeValue(nodeProf, 'text'),
            };

            // const actions = data?.actions;
            // delete data?.actions;

            let itemNew = await Item.create(
                {
                    name: `${sourceName}:${profName}`,
                    type: 'proficiency',
                    // img: 'icons/weapons/swords/sword-guard-steel-green.webp',
                    img: game.ars.config.icons.general.items['proficiency'],
                    folder: this.exportPack ? '' : this.folder.id,
                    system: {
                        ...data,
                    },
                },
                {
                    displaySheet: false,
                    pack: this.exportPack ? this.pack.collection : null,
                }
            );

            createdProfs.push({
                name: profName,
                type: 'proficiency',
                img: itemNew.img,
                id: itemNew.id,
                uuid: itemNew.uuid,
            });
        }

        return createdProfs;
    }

    async processItemRace(node) {
        let itemList = await this.processItemProficiencies(node);
        itemList = itemList.concat(await this.processItemFeatures(node, 'traits'));
        itemList = itemList.concat(await this.processItemFeatures(node));
        itemList = itemList.concat(await this.processAdvancementSkills(node, 0));

        let data = {
            name: xpath.getNodeValue(node, 'name'),
            // description: Utils.reformatMarkup(xpath.getNodeValue(node, 'text'), importData, 600),
            description: xpath.getNodeValue(node, 'text'),
            effects: Effects.getEffects(node),
            itemList: itemList,
        };

        return data;
    }

    async processItemBackground(node) {
        let itemList = await this.processItemProficiencies(node);
        itemList = itemList.concat(await this.processItemFeatures(node));
        itemList = itemList.concat(await this.processAdvancementSkills(node, 0));

        let data = {
            name: xpath.getNodeValue(node, 'name'),
            // description: Utils.reformatMarkup(xpath.getNodeValue(node, 'text'), importData, 600),
            description: xpath.getNodeValue(node, 'text'),
            effects: Effects.getEffects(node),
            itemList: itemList,
        };

        return data;
    }

    async processItemClass(node) {
        let itemList = [];

        async function processItemClassAdvancementAbilities(nodeAdvancement, className) {
            let abilItemList = [];
            const level = parseInt(xpath.getNodeValue(nodeAdvancement, 'level', '0'));

            abilItemList = abilItemList.concat(await this.processAdvancementSkills(nodeAdvancement, level));

            let actionBundle = Actions.processWeaponList(nodeAdvancement); // get weaponlist actionBundle
            Actions.processPowers('powers', nodeAdvancement, actionBundle, this.context);

            let abilitiesItem;
            if (actionBundle.length) {
                // console.log("processItemClassAdvancementAbilities CREATING", actionBundle);
                abilitiesItem = await Item.create(
                    {
                        name: `${className}: Level ${level}`,
                        type: 'ability',
                        folder: this.exportPack ? '' : this.folder.id,
                        // img: 'icons/skills/melee/hand-grip-staff-yellow-brown.webp',
                        effects: Effects.getEffects(nodeAdvancement, `${className}: Level ${level}`),
                        // system: {
                        //     actions: actionBundle,
                        // },
                    },
                    {
                        displaySheet: false,
                        pack: this.exportPack ? this.pack.collection : null,
                    }
                );

                abilitiesItem.actionGroups = Actions.makeActionsGroup(abilitiesItem, actionBundle);
                game.ars.ARSActionGroup.saveAll(abilitiesItem);

                abilItemList.push({
                    name: abilitiesItem.name,
                    type: 'ability',
                    img: abilitiesItem.img,
                    id: abilitiesItem.id,
                    uuid: abilitiesItem.uuid,
                    level: level,
                    count: 1,
                });
            }

            return abilItemList;
        }

        async function processItemClassAdvancement(nodeRanks, className) {
            let ranksBundle = [];
            // let effectsBundle = [];
            let thacoHistory = 20;
            let turnlevelHistory = 0;
            let arcaneSlotHistory = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            let divineSlotHistory = [0, 0, 0, 0, 0, 0, 0, 0];
            let saveData = {
                paralyzation: 20,
                poison: 20,
                death: 20,
                rod: 20,
                staff: 20,
                wand: 20,
                petrification: 20,
                polymorph: 20,
                breath: 20,
                spell: 20,
            };
            const _saveData = (saveInfo) => {
                let newSaveData = {
                    paralyzation:
                        saveData.paralyzation > saveInfo.paralyzation && saveInfo.paralyzation != 0
                            ? saveInfo.paralyzation
                            : saveData.paralyzation,
                    poison: saveData.poison > saveInfo.poison && saveInfo.poison != 0 ? saveInfo.poison : saveData.poison,
                    death: saveData.death > saveInfo.death && saveInfo.death != 0 ? saveInfo.death : saveData.death,
                    rod: saveData.rod > saveInfo.rod && saveInfo.rod != 0 ? saveInfo.rod : saveData.rod,
                    staff: saveData.staff > saveInfo.staff && saveInfo.staff != 0 ? saveInfo.staff : saveData.staff,
                    wand: saveData.wand > saveInfo.wand && saveInfo.wand != 0 ? saveInfo.wand : saveData.wand,
                    petrification:
                        saveData.petrification > saveInfo.petrification && saveInfo.petrification != 0
                            ? saveInfo.petrification
                            : saveData.petrification,
                    polymorph:
                        saveData.polymorph > saveInfo.polymorph && saveInfo.polymorph != 0
                            ? saveInfo.polymorph
                            : saveData.polymorph,
                    breath: saveData.breath > saveInfo.breath && saveInfo.breath != 0 ? saveInfo.breath : saveData.breath,
                    spell: saveData.spell > saveInfo.spell && saveInfo.spell != 0 ? saveInfo.spell : saveData.spell,
                };
                saveData = newSaveData;
                return newSaveData;
            };
            const _getTHACO = (thaco) => {
                if (thaco && thaco < thacoHistory) thacoHistory = thaco;

                return thacoHistory;
            };
            // const _getTurnLevel = (turnvalueentry) => {
            //     console.log("itemManager.js _getTurnLevel", turn)
            //     if (turnvalueentry && turnvalueentry > turnlevelHistory) {
            //         turnlevelHistory = turnvalueentry;
            //         console.log("itemManager.js _getTurnLevel", turnlevelHistory)
            //     }
            //     return turnlevelHistory;
            // }

            const _getArcaneSpellSlots = (slots) => {
                let newSlots = arcaneSlotHistory.map((entry, index) => {
                    return (entry += slots[index]);
                });
                arcaneSlotHistory = newSlots;
                return arcaneSlotHistory;
            };
            const _getDivineSpellSlots = (slots) => {
                let newSlots = divineSlotHistory.map((entry, index) => {
                    return (entry += slots[index]);
                });
                divineSlotHistory = newSlots;
                return divineSlotHistory;
            };

            let nodeList = xpath.getNodeList(nodeRanks, 'advancement/*');
            nodeList.sort(Utils.sortNodeByLevel);
            for (const advancementNode of nodeList) {
                let ac = xpath.getNodeValue(advancementNode, 'ac');
                if (ac == '99') {
                    ac = undefined;
                }
                let hdform = Utils._fgDiceConversion(xpath.getNodeValue(advancementNode, 'hp/dice'));
                const hdmod = parseInt(xpath.getNodeValue(advancementNode, 'hp/adjustment'));
                if (!hdform && hdmod) {
                    hdform = hdmod;
                } else if (hdmod) {
                    hdform = `${hdform}+${hdmod}`;
                }

                let baseMove = xpath.getNodeValue(advancementNode, 'speed');
                if (!baseMove) baseMove = undefined;

                const saveValues = _saveData({
                    paralyzation: parseInt(
                        xpath.getNodeValue(advancementNode, 'saves/paralyzation/base', saveData.paralyzation)
                    ),
                    poison: parseInt(xpath.getNodeValue(advancementNode, 'saves/poison/base', saveData.poison)),
                    death: parseInt(xpath.getNodeValue(advancementNode, 'saves/death/base', saveData.death)),
                    rod: parseInt(xpath.getNodeValue(advancementNode, 'saves/rod/base', saveData.rod)),
                    staff: parseInt(xpath.getNodeValue(advancementNode, 'saves/staff/base', saveData.staff)),
                    wand: parseInt(xpath.getNodeValue(advancementNode, 'saves/wand/base', saveData.wand)),
                    petrification: parseInt(
                        xpath.getNodeValue(advancementNode, 'saves/petrification/base', saveData.petrification)
                    ),
                    polymorph: parseInt(xpath.getNodeValue(advancementNode, 'saves/polymorph/base', saveData.polymorph)),
                    breath: parseInt(xpath.getNodeValue(advancementNode, 'saves/breath/base', saveData.breath)),
                    spell: parseInt(xpath.getNodeValue(advancementNode, 'saves/spell/base', saveData.spell)),
                });
                const arcaneSlotValues = _getArcaneSpellSlots([
                    0,
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/arcane/level1', 0)),
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/arcane/level2', 0)),
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/arcane/level3', 0)),
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/arcane/level4', 0)),
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/arcane/level5', 0)),
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/arcane/level6', 0)),
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/arcane/level7', 0)),
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/arcane/level8', 0)),
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/arcane/level9', 0)),
                ]);
                const divineSlotValues = _getDivineSpellSlots([
                    0,
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/divine/level1', 0)),
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/divine/level2', 0)),
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/divine/level3', 0)),
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/divine/level4', 0)),
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/divine/level5', 0)),
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/divine/level6', 0)),
                    parseInt(xpath.getNodeValue(advancementNode, 'spells/divine/level7', 0)),
                ]);

                let turnValue = parseInt(xpath.getNodeValue(advancementNode, 'turnlevel', '0'));
                if (!turnValue) turnValue = undefined;

                itemList = itemList.concat(await processItemClassAdvancementAbilities.bind(this)(advancementNode, className));

                // effectsBundle.push(Effects.getEffects(advancementNode));

                let advancementData = {
                    ac: ac,
                    level: parseInt(xpath.getNodeValue(advancementNode, 'level')),
                    xp: parseInt(xpath.getNodeValue(advancementNode, 'expneeded')),
                    hdformula: hdform,
                    baseMove: baseMove,
                    thaco: _getTHACO(parseInt(xpath.getNodeValue(advancementNode, 'thaco'))),
                    turnLevel: turnValue,
                    ...saveValues,
                    divine: divineSlotValues,
                    arcane: arcaneSlotValues,
                    casterlevel: {
                        arcane: parseInt(xpath.getNodeValue(advancementNode, 'arcane/totallevel', 0)),
                        divine: parseInt(xpath.getNodeValue(advancementNode, 'divine/totallevel', 0)),
                    },
                };

                ranksBundle.push(advancementData);
            }

            // const allData = { ranks: ranksBundle, effects: effectsBundle };
            return ranksBundle;
        }

        const ranksBundle = await processItemClassAdvancement.bind(this)(node, xpath.getNodeValue(node, 'name'));

        itemList = itemList.concat(await this.processItemFeatures(node));

        let data = {
            name: xpath.getNodeValue(node, 'name'),
            // description: Utils.reformatMarkup(xpath.getNodeValue(node, 'text'), importData, 600),
            description: xpath.getNodeValue(node, 'text'),
            ranks: ranksBundle,
            itemList: itemList,
            proficiencies: {
                penalty: parseInt(xpath.getNodeValue(node, 'weapon_penalty', 0)),
                weapon: {
                    starting: parseInt(xpath.getNodeValue(node, 'profs/initial/weapon', 0)),
                    earnLevel: parseInt(xpath.getNodeValue(node, 'profs/rate/weapon', 0)),
                },
                skill: {
                    starting: parseInt(xpath.getNodeValue(node, 'profs/initial/nonweapon', 0)),
                    earnLevel: parseInt(xpath.getNodeValue(node, 'profs/rate/nonweapon', 0)),
                },
            },
        };

        return data;
    }

    processItemSkill(node) {
        // look for "Smithing [2]" and take cost from [\d+]
        const _calculateCost = (name) => {
            let cost = 1;

            const match = name.match(/\[(\d+)\]/);
            if (match && match[1]) {
                cost = parseInt(match[1]);
            }
            return cost;
        };
        const _convertAbility = (ability) => {
            let abl = 'none';
            if (ability) {
                abl = ability.substring(0, 3);
            }
            return abl;
        };
        const _cleanName = (name) => {
            let newName = name.replace(/\[\d+\]/, '');
            return newName.trim();
        };

        let type = 'decending';
        let formula = '1d20';
        let ability = _convertAbility(xpath.getNodeValue(node, 'stat'));
        const fgStat = xpath.getNodeValue(node, 'stat');
        if (fgStat && fgStat === 'percent') {
            ability = 'none';
            type = 'decending';
            formula = '1d100';
        }
        let data = {
            name: _cleanName(xpath.getNodeValue(node, 'name')),
            // description: Utils.reformatMarkup(xpath.getNodeValue(node, 'text'), importData, 600),
            description: xpath.getNodeValue(node, 'text'),
            features: {
                value: xpath.getNodeValue(node, 'base_check'),
                formula: formula,
                type: type,
                ability: ability,
                cost: _calculateCost(xpath.getNodeValue(node, 'name')),
                modifiers: {
                    formula: xpath.getNodeValue(node, 'adj_mod'),
                },
            },
            // img:
        };

        return data;
    }

    processItemSpell(itemNode) {
        const _getComponents = (comp) => {
            let verbal = new RegExp('V').test(comp);
            let somatic = new RegExp('S').test(comp);
            let material = new RegExp('M').test(comp);

            return { verbal: verbal, somatic: somatic, material: material };
        };

        function processSpellActions(itemNode) {
            let actionBundle = [];

            const powerInfo = {
                spellType: xpath.getNodeValue(itemNode, 'type'),
                powerName: xpath.getNodeValue(itemNode, 'name'),
                powerSpeed: xpath.getNodeValue(itemNode, 'castingtime'),
                powerPrepared: 0,
            };

            Actions.processActions(itemNode, 'actions', powerInfo, actionBundle);
            return actionBundle;
        }

        const components = _getComponents(xpath.getNodeValue(itemNode, 'components'));
        let spellData = {
            name: xpath.getNodeValue(itemNode, 'name'),
            type: xpath.getNodeValue(itemNode, 'type'),
            level: xpath.getNodeValue(itemNode, 'level'),
            school: xpath.getNodeValue(itemNode, 'school'),
            sphere: xpath.getNodeValue(itemNode, 'sphere'),
            range: xpath.getNodeValue(itemNode, 'range'),
            components: {
                verbal: components.verbal,
                somatic: components.somatic,
                material: components.material,
            },
            durationText: xpath.getNodeValue(itemNode, 'duration'),
            castingTime: xpath.getNodeValue(itemNode, 'castingtime'),
            areaOfEffect: xpath.getNodeValue(itemNode, 'aoe'),
            save: xpath.getNodeValue(itemNode, 'save'),
            source: xpath.getNodeValue(itemNode, 'source'),
            // description: Utils.reformatMarkup(xpath.getNodeValue(itemNode, 'description'), importData, 600),
            description: xpath.getNodeValue(itemNode, 'description'),
            actions: processSpellActions(itemNode),
        };
        return spellData;
    }

    /**
     *
     * Process weapon parts of a item
     *
     * @param {*} node
     */
    processWeaponItem(node, itemData) {
        let img = 'icons/skills/melee/hand-grip-sword-white-brown.webp';

        let damageText = xpath.getNodeValue(node, 'damage');
        let damageList = damageText.split(';');
        let dmg = [];
        let foundLarge = false;
        if (damageList.length) {
            for (const entry of damageList) {
                let dmgEntry = Utils._cleanDamage(entry);
                let dmgType = Utils._cleanDamageType(entry);
                if (dmg.length < 1) {
                    dmg.push({
                        dmg: dmgEntry,
                        type: dmgType,
                    });
                } else if (entry.match(/large/gi)) {
                    dmg.push({
                        dmg: dmgEntry,
                        type: dmgType,
                    });
                    foundLarge = true;
                    break;
                }
            }
        }

        let weaponType = 'melee';
        if (itemData.name.match(/bow/i)) {
            weaponType = 'ranged';
            img = 'icons/skills/ranged/arrow-barbed-flying-gray.webp';
        }
        if (itemData.name.match(/throw|hurl|jav/i)) {
            weaponType = 'thrown';
            img = 'icons/skills/ranged/dagger-thrown-jeweled-green.webp';
        }

        let weaponData = {
            damage: {
                normal: dmg[0].dmg,
                large: foundLarge ? dmg[1].dmg : '',
                type: dmg[0].type,
            },
            attack: {
                speed: itemData?.actions[0]?.speed ? itemData.actions[0].speed : xpath.getNodeValue(node, 'speedfactor'),
                type: weaponType,
            },
        };

        if (weaponType === 'melee') {
            switch (weaponData.damage.type) {
                case 'slashing':
                    img = 'icons/weapons/swords/shortsword-guard-brass.webp';
                    break;
                case 'piercing':
                    img = 'icons/weapons/daggers/dagger-straight-cracked.webp';
                    break;
                case 'bludgeoning':
                    img = 'icons/weapons/clubs/club-banded-steel.webp';
                    break;

                default:
                    itemData.img = img;
                    break;
            }
        }
        itemData.img = img;
        return weaponData;
    }

    /**
     *
     * Import the armor section of a item
     *
     * @param {*} node
     * @param {*} itemData
     * @returns armorData
     *
     */
    processArmorItem(node, itemData) {
        const nodeType = itemData.attributes.type.toLowerCase();
        const nodeSubType = itemData.attributes.subtype.toLowerCase();
        let armorType = 'armor';
        let img = 'icons/equipment/chest/breastplate-banded-steel-studded.webp';

        const typeList = [nodeType, nodeSubType];
        if (typeList.includes('shield')) {
            armorType = 'shield';
            img = 'icons/skills/melee/shield-block-gray-orange.webp';
        } else if (typeList.includes('ring')) {
            armorType = 'ring';
            img = 'icons/equipment/finger/ring-band-notched-gold.webp';
        } else if (typeList.includes('cloak')) {
            armorType = 'cloak';
            img = 'icons/equipment/back/cloak-brown-fur-brown.webp';
        } else if (typeList.includes('warding')) {
            armorType = 'warding';
            img = 'icons/equipment/wrist/bracer-studded-leather-steel.webp';
        } else if (typeList.includes('other')) {
            armorType = 'other';
        }

        itemData.img = img;

        let armorData = {
            protection: {
                type: armorType,
                ac: parseInt(xpath.getNodeValue(node, 'ac', 10)),
                modifier: parseInt(xpath.getNodeValue(node, 'bonus', 0)),
                points: {
                    min: 0,
                    max: parseInt(xpath.getNodeValue(node, 'armor/dp/base', 0)),
                    value: parseInt(xpath.getNodeValue(node, 'armor/dp/base', 0)),
                },
            },
        };

        return armorData;
    }

    /**
     *
     * Import any generic 'Item'
     *
     * @param {*} node
     */
    processGearItem(node) {
        function processItemActions(itemNode) {
            const powerInfo = {
                spellType: '',
                powerName: xpath.getNodeValue(itemNode, 'name', 'UNKNOWN'),
                powerSpeed: xpath.getNodeValue(itemNode, 'speedfactor'),
                powerPrepared: 0,
            };

            let actionBundle = Actions.processWeaponList(itemNode); // get weaponlist actionBundle
            // Actions.processPowers('powers', itemNode, actionBundle, this.context);
            Actions.processPowers('powers', itemNode, actionBundle);
            Actions.processActions(itemNode, 'actions', powerInfo, actionBundle);
            return actionBundle;
        }

        const _isMagic = (node) => {
            let magic = false;
            const nodeType = xpath.getNodeValue(node, 'type').toLowerCase();
            const nodeSubType = xpath.getNodeValue(node, 'subtype').toLowerCase();
            magic = nodeType === 'magic' || nodeSubType === 'magic';
            // console.log("_isMagic ====", { magic, nodeType, nodeSubType })
            return magic;
        };

        const priceText = xpath.getNodeValue(node, 'cost', '0 cp');
        const [itemValue, itemCurrency] = priceText.split(' ');

        let data = {
            name: xpath.getNodeValue(node, 'name', 'UNKNOWN'),
            source: xpath.getNodeValue(node, 'source'),
            // description: Utils.reformatMarkup(xpath.getNodeValue(node, 'description'), importData, 300),
            description: xpath.getNodeValue(node, 'description'),
            // ???: xpath.getNodeValue(node, 'nonidentified'),
            // dmonlytext: Utils.reformatMarkup(xpath.getNodeValue(node, 'dmonly'), importData, 300),
            dmonlytext: xpath.getNodeValue(node, 'dmonly'),
            cost: {
                value: parseInt(itemValue) || 0,
                currency: itemCurrency,
            },
            count: xpath.getNodeValue(node, 'count'),
            xp: xpath.getNodeValue(node, 'exp'),
            weight: xpath.getNodeValue(node, 'weight'),
            quantity: xpath.getNodeValue(node, 'quantity'),

            actions: processItemActions(node),
            effects: Effects.getEffects(node),

            attributes: {
                type: xpath.getNodeValue(node, 'type'),
                subtype: xpath.getNodeValue(node, 'subtype'),
                identified: xpath.getNodeValue(node, 'isidentified', '1') === '1',
                rarity: xpath.getNodeValue(node, 'rarity'),
                magic: _isMagic(node),
            },
        };

        return data;
    }

    /**
     *
     * Pass item xml node with type and return itemData for creation
     *
     * @param {*} node
     * @param {*} itemType
     * @returns
     */
    async getItemData(node, itemType) {
        itemType = itemType.toLowerCase();
        let itemData;
        console.log('Items.getItemData', itemType, node);
        switch (itemType) {
            case 'spell':
                itemData = this.processItemSpell(node);
                itemData.img = 'icons/sundries/scrolls/scroll-bound-black-brown.webp';
                break;
            case 'weapon':
                itemData = this.processGearItem(node);
                let weaponData = this.processWeaponItem(node, itemData);
                itemData = {
                    ...itemData,
                    ...weaponData,
                };
                break;
            case 'armor':
                itemData = this.processGearItem(node);
                let armorData = this.processArmorItem(node, itemData);
                itemData = {
                    ...itemData,
                    ...armorData,
                };
                break;
            case 'potion':
                itemData = this.processGearItem(node);
                itemData.img = 'icons/consumables/potions/potion-tube-corked-orange.webp';
                break;

            case 'skill':
                itemData = this.processItemSkill(node);
                itemData.img = 'icons/skills/melee/unarmed-punch-fist.webp';
                break;

            case 'class':
                itemData = await this.processItemClass(node);
                itemData.img = 'icons/tools/scribal/spectacles-glasses.webp';
                break;

            case 'race':
                itemData = await this.processItemRace(node);
                itemData.img = 'icons/skills/movement/figure-running-gray.webp';
                break;

            case 'background':
                itemData = await this.processItemBackground(node);
                itemData.img = 'icons/tools/smithing/tongs-steel-grey.webp';
                break;

            default:
                itemData = this.processGearItem(node);
                itemData.actualType = 'item';
                break;
        }

        return itemData;
    }
    /**
     *
     * @param {*} node
     * @param {*} itemType
     * @returns
     */
    async import(node, itemType) {
        // console.log('itemManager.js importItem', { node }, this);

        let itemData = await this.getItemData(node, itemType);
        let itemNew;
        if (itemData?.name && itemData?.name != 'UNKNOWN') {
            if (itemData) {
                const itemName = itemData.name;
                const itemEffects = itemData.effects;
                let img = game.ars.config.icons.general.items[itemType] ?? itemData.img;
                const actions = itemData.actions;
                // dont want this dangling as data.name
                delete itemData.name;
                delete itemData.effects;
                delete itemData.img;
                delete itemData.actions;

                const nodeType = itemData.attributes?.type?.toLowerCase() ?? 'item';
                const nodeSubType = itemData.attributes?.subtype?.toLowerCase() ?? '';
                const typeList = [nodeType, nodeSubType];
                if (typeList.includes('ring')) {
                    img = 'icons/equipment/finger/ring-band-worn-gold.webp';
                } else if (typeList.includes('scroll')) {
                    img = 'icons/sundries/scrolls/scroll-bound-blue-tan.webp';
                } else if (itemName.match(/key/i)) {
                    img = 'icons/sundries/misc/key-gold.webp';
                }

                if (itemName) {
                }
                itemNew = await Item.create(
                    {
                        name: itemName,
                        type: itemType,
                        // img: img,
                        img,
                        effects: itemEffects,
                        folder: this.exportPack ? '' : this.folder.id,
                        system: {
                            ...itemData,
                        },
                    },
                    {
                        displaySheet: false,
                        pack: this.exportPack ? this.pack.collection : null,
                    }
                );

                // convert the old style actions to new ones
                itemNew.actionGroups = game.ars.ARSAction.convertFromActionBundle(itemNew, actions);
                await game.ars.ARSActionGroup.saveAll(itemNew);

                // console.log(`Done importing ${itemName}`, itemNew);
            } else {
                // console.log(`Cound not import `, { node, itemType, itemData });
            }
            if (itemNew) {
                const entry = {
                    nodeId: node.nodeName,
                    type: itemType,
                    name: itemNew.name,
                    pack: this.exportPack ? this.pack.collection : null,
                    id: itemNew.id,
                    img: itemNew.img,
                    uuid: itemNew.uuid,
                };
                if (!this.importData.itemImported) this.importData.itemImported = [];
                this.importData.itemImported.push(entry);

                if (!this.importData.imported[itemType]) this.importData.imported[itemType] = [];
                this.importData.imported[itemType].push(entry);

                this.context.maps.items.set(node.nodeName, itemNew);
            }

            let count = parseInt(itemNew.system.count);
            if (isNaN(count)) count = 1;

            return {
                name: itemNew.name,
                nodeId: node.nodeName,
                id: itemNew.id,
                type: itemType,
                img: itemNew.img,
                pack: this.exportPack ? this.pack.collection : null,
                // count: parseInt(itemNew.system.count) ?? 1,
                count,
            };
        }
    }
} // end class Items
