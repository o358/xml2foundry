import XmlImporter from './xml-import.js';
import * as xpath from './xpath.js';
import { Utils } from './utilities.js';

/**
 *
 */
export class Scenes {
    /**
     *
     * @param {*} importData
     * @param {*} node
     * @param {*} folder
     * @param {*} sortNumber
     * @param {*} options
     */
    // node, folder, this.importData, sortNumber
    constructor(node, folder, context, sortNumber, options = {}) {
        this.context = context;
        this.importData = context.importData;
        this.node = node;
        this.folder = folder;
        this.sortNumber = sortNumber;
        this.options = options;
    }

    getSceneImage() {
        let img = '';

        let nodeList = xpath.getNodeList(this.node, `image/layers/*`);
        for (const nodeLayer of nodeList) {
            const bitmap = xpath.getNodeValue(nodeLayer, 'bitmap');
            if (bitmap && !img) img = bitmap.replace('campaign/', '');
            const visible = xpath.getNodeValue(nodeLayer, 'visible', 'everyone');
            // console.log('sceneManager.js getSceneImage FOUND Image: ', [bitmap, visible]);
            if (visible === 'everyone' && bitmap) {
                let imgPrep = bitmap.replace('campaign/', '');
                imgPrep = imgPrep.replace(/@.*$/, '');
                // console.log('sceneManager.js getSceneImage VISIBLE setting Image: ', [nodeLayer, bitmap, imgPrep]);
                img = imgPrep;
                break;
            }
        }

        // console.log('sceneManager.js getSceneImage', [img]);
        return img;
    }

    async importScene() {
        const sceneMap = new Map();
        // find the first image file that is visible to all (fg can have multiple images in a layer)
        const name = xpath.getNodeValue(this.node, 'name', 'UNKNOWN');
        const gridStatus = xpath.getNodeValue(this.node, 'image/grid', 'off') === 'on';
        const gridSizes = xpath.getNodeValue(this.node, 'image/gridsize');
        const gridTypeName = xpath.getNodeValue(this.node, 'image/gridtype', 'grid'); // hexcolumn, hexrow, if empty, grid
        const suffix = xpath.getNodeValue(this.node, 'image/distancesuffix', 'ft');
        const distance = xpath.getNodeValue(this.node, 'image/distancebaseunit', 10);
        let gridType = 1; // we as default to grid
        switch (gridTypeName) {
            case 'hexcolumn':
                gridType = 2;
                break;
            case 'hexrow':
                gridType = 3;
                break;
        }
        let [gridSize, ...rest] = gridSizes.split(',');
        gridSize = parseInt(gridSize);
        console.log('sceneManager.js importScene gridSize', {
            name,
            gridStatus,
            gridSizes,
            gridSize,
            gridType,
        });
        if (gridSize || gridStatus) {
            if (isNaN(gridSize)) gridSize = 50;
            // wait while canvas is loading...
            while (canvas.loading) {
                await Utils.sleep(500);
            }
            const sceneImage = this.getSceneImage();

            if (sceneImage) {
                let scene = await Scene.create(
                    {
                        // console.log({
                        name: Utils.cleanSortingName(name),
                        navigation: false, // don't default to show in navigation and fill up the screen
                        folder: this.folder.id,
                        sort: this.sortNumber,
                        grid: gridSize < 50 ? 50 : gridSize,
                        gridType: gridType,
                        width: null,
                        height: null,
                        img: `${this.importData.importPath}/${this.importData.importName}/${sceneImage}`,
                        // }, { displaySheet: false });
                    },
                    { displaySheet: false }
                );

                this.context.maps.scenes.set(this.node.nodeName, scene);
                let thumb = await scene.createThumbnail();
                scene.update({ thumb: thumb.thumb });
                // scene.update({ "thumb": thumb.thumb, "height": thumb.height, "width": thumb.width }, { diff: true });
                if (!this.importData.imported['scene']) this.importData.imported['scene'] = [];
                this.importData.imported['scene'].push({
                    nodeId: this.node.nodeName,
                    type: 'scene',
                    name: scene.name,
                    id: scene.id,
                    img: scene.img,
                });
            } else {
                console.log('sceneManager.js importScene SKIPPING SCENE Named (no image)', name);
            }
        } else {
            console.log('sceneManager.js importScene SKIPPING SCENE Named (no gridType)', name);
        }
        return this;
    }
} // end class scenes
