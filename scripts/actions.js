import * as xpath from './xpath.js';
import { Items } from './itemManager.js';
import { Utils } from './utilities.js';
import { Effects } from './effectsManager.js';

export class Actions {
    static processActions(actionsNode, path, powerInfo, actionBundle) {
        const _actionTypeFinal = (sType, sAtkType) => {
            let final = sType;
            if (sAtkType === 'melee') {
                final = 'melee';
            } else if (sAtkType === 'ranged') {
                final = 'ranged';
            } else {
                final = sType;
            }
            return final;
        };
        const _atkStatFinal = (sAtkStat) => {
            let final = 'none';
            if (sAtkStat && sAtkStat != 'none') {
                let matching = sAtkStat.match(/^([a-z]{0,3})/i);
                final = matching[0];
            }
            return final;
        };
        const _successAction = (sSuccess) => {
            let success = 'none';
            if (sSuccess && sSuccess === 'half') {
                success = 'halve';
            }
            return success;
        };
        const _formula = (durcalc = false, formula, dice, diceCustom, casterType, customValue, castermax, spellType, bonus) => {
            let sType = spellType.toLowerCase();
            let useLevelOnly = false;
            let level = parseInt(sType);
            let casterLevelFormula = `@rank.levels.${sType}`;
            // console.log("_formula sType", { sType, level });
            if (Number.isInteger(level)) {
                useLevelOnly = true;
                casterLevelFormula = useLevelOnly ? `${level}` : `@rank.levels.${sType}`;
            }

            if (sType) {
                if (casterType) {
                    if (casterType.match(/casterlevel/i)) {
                        if (customValue) {
                            if (castermax) {
                                formula = `(min(${castermax},` + casterLevelFormula + `))*${customValue}`;
                            } else {
                                formula = `(` + casterLevelFormula + `)*${customValue}`;
                            }
                        } else {
                            if (castermax) {
                                formula = `(min(${castermax},` + casterLevelFormula + `))${dice}`;
                            } else {
                                if (durcalc) {
                                    formula = `${dice}`;
                                } else {
                                    formula = casterLevelFormula + `${dice}`;
                                }
                            }
                        }
                    } else if (casterType.match(/casterlevelby\d+/i)) {
                        const divideLevel = casterType.match(/casterlevelby(\d+)/i);
                        const nPer = parseInt(divideLevel);
                        if (castermax) {
                            formula = `(floor(min(${castermax},` + casterLevelFormula + `))/${nPer})${dice}`;
                        } else {
                            formula = `(floor(` + casterLevelFormula + `)/${nPer})${dice}`;
                        }
                    }
                    // formula = formula + (diceCustom ? `+${diceCustom}` : "") + (bonus ? `+${bonus}` : "");
                }
            }
            formula = formula + (diceCustom ? `+${diceCustom}` : '') + (bonus ? `+${bonus}` : '');

            return formula;
        };

        let speed = powerInfo.powerSpeed;
        if (speed) {
            // if the speed takes a round+ then it's +99
            if (speed.match(/round|rnd|turn|trn|day|hour/i)) {
                speed = 99;
            } else {
                // attempt to grab the first number and use it
                const match = speed.match(/(\d+)/);
                if (match && match[1]) {
                    speed = parseInt(match[1]);
                }
            }
        }
        for (const actionNode of xpath.queryXPathAll(actionsNode, `${path}/*`)) {
            const actionType = xpath.getNodeValue(actionNode, 'type');
            const actionSaveType = xpath.getNodeValue(actionNode, 'savetype');
            const onSuccess = xpath.getNodeValue(actionNode, 'onmissdamage');
            const atktype = xpath.getNodeValue(actionNode, 'atktype'); // melee or ranged
            const atkstat = xpath.getNodeValue(actionNode, 'atkstat'); //strength or dexterity
            const atkbase = xpath.getNodeValue(actionNode, 'atkbase'); //ability
            const atkmod = xpath.getNodeValue(actionNode, 'atkmod'); //#
            // const action = new game.ars.ARSAction({})
            // let action = game.ars.actionManager.createActionEntry(null, actionBundle.length);
            let action = new game.ars.ARSAction(undefined, {});
            actionBundle.push(action);
            action.name = powerInfo.powerName;
            action.speed = speed;
            action.properties = String.prototype.split(xpath.getNodeValue(actionNode, 'properties'), ',');
            action.successAction = _successAction(onSuccess);
            action.saveCheck.type = actionSaveType ? actionSaveType : 'none';
            action.type = _actionTypeFinal(actionType, atktype);
            action.ability = _atkStatFinal(atkstat);
            action.description = powerInfo.powerDesc;
            if (actionType === 'cast') {
                if (powerInfo.powerPrepared > 0) {
                    action.resource.type = 'charged';
                    action.resource.reusetime = '';
                    action.resource.count.max = parseInt(powerInfo.powerPrepared);
                    action.resource.count.value = 0;
                }
            } else if (actionType === 'effect') {
                const durdice = xpath.getNodeValue(actionNode, 'durdice');
                const durmod = parseInt(xpath.getNodeValue(actionNode, 'durmod', '0'));
                let durunit = xpath.getNodeValue(actionNode, 'durunit'); // minute|hour|day "" = round
                const durvalue = parseInt(xpath.getNodeValue(actionNode, 'durvalue', '0'));
                const label = xpath.getNodeValue(actionNode, 'label');
                const visibility = xpath.getNodeValue(actionNode, 'visibility');
                const casterType = xpath.getNodeValue(actionNode, 'castertype'); // casterlevelby(%d+) (level/X) or casterlevel
                const castermax = xpath.getNodeValue(actionNode, 'castermax');
                switch (durunit) {
                    case 'minute':
                        durunit = 'turn';
                        break;
                    case 'hour':
                        break;
                    case 'day':
                        break;
                    default:
                        durunit = 'round';
                }
                action.effect.duration.type = durunit;
                action.effect.duration.formula = _formula(
                    true,
                    `${Utils._fgDiceConversion(durdice)}`,
                    Utils._fgDiceConversion(durdice),
                    '',
                    casterType,
                    durvalue,
                    parseInt(castermax),
                    powerInfo.spellType,
                    durmod
                );
                // action.effect.changes = _effect(label);
                action.effect.changes = Effects._effect(label);
                // action.effect.changes.push({
                //     key: "SPELL",
                //     value: label,
                //     mode: 0,
                // });
            } else if (actionType === 'damage' || actionType === 'heal') {
                const nodelistPath = actionType === 'damage' ? 'damagelist' : 'heallist';
                let firstDamage = true;
                for (const damageNode of xpath.queryXPathAll(actionNode, `${nodelistPath}/*`)) {
                    let dice = xpath.getNodeValue(damageNode, 'dice');
                    let diceCustom = xpath.getNodeValue(damageNode, 'dicecustom');
                    const bonus = parseInt(xpath.getNodeValue(damageNode, 'bonus'));
                    const customValue = parseInt(xpath.getNodeValue(damageNode, 'customvalue'));
                    const casterType = xpath.getNodeValue(damageNode, 'castertype'); // casterlevelby(%d+) (level/X) or casterlevel
                    const castermax = xpath.getNodeValue(damageNode, 'castermax');
                    const damageType = xpath.getNodeValue(damageNode, 'type');

                    if (!dice && diceCustom) {
                        dice = diceCustom;
                        diceCustom = '';
                    }
                    let formula = _formula(
                        false,
                        `${Utils._fgDiceConversion(dice)}`,
                        Utils._fgDiceConversion(dice),
                        Utils._fgDiceConversion(diceCustom),
                        casterType,
                        customValue,
                        parseInt(castermax),
                        powerInfo.spellType,
                        bonus
                    );

                    if (firstDamage) {
                        firstDamage = false;
                        action.formula = formula;
                        action.damagetype = Utils._cleanDamageType(damageType);
                    } else {
                        action.otherdmg.push({
                            formula: formula,
                            type: Utils._cleanDamageType(damageType),
                        });
                    }
                }
            }
            action.img = game.ars.config.icons.general.combat[action.type];
        }
    }
    /**
     *
     * Generalized function to parse out actions from FG to Foundry OSRIC
     *
     * @param {String} nodePath Path to search for actions powers would search powers/*
     * @param {Object} node XML content object
     * @param {Array} actionBundle Array of actions
     * @returns
     */
    static processPowers(nodePath, node, actionBundle, context) {
        for (const powerNode of xpath.queryXPathAll(node, `${nodePath}/*`)) {
            const spellType = xpath.getNodeValue(powerNode, 'type').toLowerCase(); // arcane or divine
            const powerName = xpath.getNodeValue(powerNode, 'name');
            const powerSpeed = xpath.getNodeValue(powerNode, 'castinitiative');
            const powerPrepared = parseInt(xpath.getNodeValue(powerNode, 'prepared', '0'));
            const powerDesc = Utils.reformatMarkup(xpath.getNodeValue(powerNode, 'description'), context, 600);

            const powerInfo = {
                spellType: spellType,
                powerName: powerName,
                powerSpeed: powerSpeed,
                powerPrepared: powerPrepared,
                powerDesc: powerDesc,
            };

            Actions.processActions(powerNode, 'actions', powerInfo, actionBundle);
        }

        return actionBundle;
    }

    /**
     *
     * Create actions from weaponlist entries, executed from within processPowers()
     *
     * @param {*} npcNode
     */
    static processWeaponList(weaponListNode) {
        let attacks = [];
        for (const weaponNode of xpath.queryXPathAll(weaponListNode, 'weaponlist/*')) {
            let dmgList = [];
            const attackName = xpath.getNodeValue(weaponNode, 'name');
            const speed = xpath.getNodeValue(weaponNode, 'speedfactor', 0);
            const type = xpath.getNodeValue(weaponNode, 'type', 0);
            for (const dmgNode of xpath.queryXPathAll(weaponNode, 'damagelist/*')) {
                let dmg = {
                    dmg: Utils._fgDiceConversion(xpath.getNodeValue(dmgNode, 'dice')),
                    dmgType: Utils._cleanDamageType(xpath.getNodeValue(dmgNode, 'type')),
                };
                dmgList.push(dmg);
            }
            let attack = {
                name: attackName,
                dmg: dmgList,
                speed: speed,
            };
            attacks.push(attack);
        }

        let actionBundle = [];
        attacks.forEach((attack, index) => {
            this.createActionForActionBundle(actionBundle, attack.dmg, attack.name, attack.speed);
        });
        return actionBundle;
    }

    /**
     * Make actionsGroup from actionBundle
     *
     * @param {*} source
     * @param {*} actionBundle
     */
    static makeActionsGroup(source, actionBundle) {
        const actionGroups = new Map();
        if (actionBundle?.length) {
            // sort actions by name
            const groupActionsByName = (actions) => {
                return actions.reduce((acc, action) => {
                    // Check if the group already exists
                    if (!acc[action.name]) {
                        acc[action.name] = [];
                    }
                    // Add the action to the appropriate group
                    acc[action.name].push(action);
                    return acc;
                }, {});
            };
            const groupedActions = groupActionsByName(actionBundle);
            // iterate over actionName
            for (const actionName in groupedActions) {
                if (groupedActions.hasOwnProperty(actionName)) {
                    //   console.log(`Actions with name '${actionName}':`);
                    //make new group for action name
                    const newGroup = new game.ars.ARSActionGroup(source, { name: actionName });
                    groupedActions[actionName].forEach((action) => {
                        // put all actions in same group
                        newGroup.add(action);
                    });
                    actionGroups.set(newGroup.id, newGroup);
                }
            }
        }
        return actionGroups;
    }

    /**
     *
     * @param {*} actionBundle
     * @param {*} damageList
     * @param {*} actionName
     * @param {*} actionSpeed
     * @returns
     */
    static async createActionForActionBundle(actionBundle, damageList, actionName = 'Attack', actionSpeed = 0) {
        // console.log('createActionForActionBundle', { actionBundle, damageList, actionName });
        if (damageList && damageList.length) {
            this.createActionFromDamage(null, damageList, actionBundle, actionName, actionSpeed);
            return actionBundle;
        }
    }

    /**
     *
     * @param {*} object
     * @param {*} damageList
     * @param {*} actionBundle
     * @param {*} actionName
     * @param {*} actionSpeed
     * @returns
     */
    static createActionFromDamage(object, damageList, actionBundle, actionName = 'Attack', actionSpeed = 0) {
        // create "attack" action first, default to melee/str based
        // let action = createActionEntry(object, actionBundle.length);
        let action = new game.ars.ARSAction(undefined, {});
        action.name = actionName;
        action.speed = actionSpeed;
        action.type = 'melee';
        action.ability = 'none';
        action.abilityCheck.type = 'none';
        action.saveCheck.type = 'none';
        action.img = game.ars.config.icons.general.combat[action.type];
        actionBundle.push(action);

        const _actionDamage = (dmg, dmgType = 'slashing') => {
            let actionDamage;
            if (dmg) {
                // actionDamage = createActionEntry(object, actionBundle.length);
                actionDamage = new game.ars.ARSAction(undefined, {});
                let diceString = '';
                try {
                    const match = dmg?.match(/^(\d+[\-]\d+$)/) || null;
                    if (match && match[1]) diceString = match[1];
                } catch (err) {}

                if (diceString) {
                    // diceString = utilitiesManager.diceFixer(diceString[0]);
                    diceString = utilitiesManager.diceFixer(diceString);
                } else {
                    diceString = dmg;
                }

                actionDamage.name = actionName;
                actionDamage.damagetype = dmgType;
                actionDamage.type = 'damage';
                actionDamage.ability = 'none';
                actionDamage.formula = diceString;
                actionDamage.abilityCheck.type = 'none';
                actionDamage.saveCheck.type = 'none';
                actionDamage.img = game.ars.config.icons.general.combat[action.type];
            } else {
                // console.log("createActionFromDamage", { object, dmg, actionBundle });
            }
            return actionDamage;
        };

        // create damage entries for each valid damage dice we find in string
        damageList.forEach((entry) => {
            if (entry.dmg) {
                actionBundle.push(_actionDamage(entry.dmg, entry.dmgType));
            } else {
                actionBundle.push(_actionDamage(entry));
            }
        });

        return actionBundle;
    }
} // end actions class
