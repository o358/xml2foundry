import XmlImporter from './xml-import.js';
import * as xpath from './xpath.js';
import { Actions } from './actions.js';
import { Effects } from './effectsManager.js';
import { Items } from './itemManager.js';
import { Utils } from './utilities.js';

export class Actors {
    constructor(npcNode, exportPack, actorPack, actorFolder, context, options = {}) {
        this.context = context;
        this.importData = context.importData;
        this.npcNode = npcNode;
        this.actorFolder = actorFolder;
        this.exportPack = exportPack;
        this.actorPack = actorPack;
        this.options = options;
    }

    // Items.processInventory('inventorylist', actorNode);
    // processInventory(nodePath,actorNode) {
    async processNPCInventory(node) {
        const inventoryList = [];
        for (const itemNode of xpath.queryXPathAll(node, `inventorylist/*`)) {
            let itemType = xpath.getNodeValue(itemNode, 'type').toLowerCase();
            const itemInfo = new Items(node, itemType, this.exportPack, this.pack, this.folder, this.context);

            let item = await itemInfo.getItemData(itemNode, itemType);
            if (item) {
                const nodeType = item.attributes.type.toLowerCase();
                const nodeSubType = item.attributes.subtype.toLowerCase();
                const typeList = [nodeType, nodeSubType];

                const name = item.name;
                let img = item.img?.replace(/@.*$/, '') ?? '';
                if (typeList.includes('ring')) {
                    img = 'icons/equipment/finger/ring-band-worn-gold.webp';
                } else if (typeList.includes('scroll')) {
                    img = 'icons/sundries/scrolls/scroll-bound-blue-tan.webp';
                } else if (name.match(/key/i)) {
                    img = 'icons/sundries/misc/key-gold.webp';
                }

                const actions = item.actions;
                const effects = item.effects;
                const type = item.actualType ? item.actualType : itemType ?? 'item';

                delete item.name;
                delete item.img;
                delete item.actions;
                delete item.actualType;
                delete item.effects;

                inventoryList.push(foundry.utils.mergeObject({}, { type, effects, name, img, actions, system: { ...item } }));
            }
        }

        return inventoryList;
    }

    /**
     *
     * @param {*} npcNode
     * @param {*} sortNumber
     */
    async import(npcNode, sortNumber) {
        const npcData = this.processNPC(npcNode, this.importData);
        console.log('actorManager.js import npcData', npcData);

        const inventoryList = await this.processNPCInventory(npcNode);

        const npcName = npcData?.name ?? 'Unknown-Actor-Name';
        const npcImg = npcData.img.replace(/@.*$/, '');
        const actions = npcData.actions;
        // dont want this dangling as data.name
        delete npcData.name;
        delete npcData.img;
        delete npcData.actions;
        delete npcData.inventoryList;

        if (npcName) {
            let actor = await Actor.create(
                {
                    name: npcName ?? 'Unknow-ActorName',
                    type: 'npc',
                    img: npcImg ?? 'icons/svg/mystery-man.svg',
                    folder: this.exportPack ? '' : this.actorFolder.id,
                    sort: sortNumber,
                    system: {
                        ...npcData,
                    },
                    effects: Effects.getEffects(npcNode),
                },
                {
                    renderSheet: false,
                    pack: this.exportPack ? this.actorPack.collection : null,
                }
            );
            // }, { temporary: false, displaySheet: false, pack: pack.collection });

            actor.actionGroups = Actions.makeActionsGroup(actor, actions);
            game.ars.ARSActionGroup.saveAll(actor);

            //TODO: this should really just come in as the new style.
            // convert the old style actions to new ones
            actor.actionGroups = game.ars.ARSAction.convertFromActionBundle(actor, actor.system.actions);
            await game.ars.ARSActionGroup.saveAll(actor);

            // if (inventoryList.length) {
            //     await actor.createEmbeddedDocuments('Item', inventoryList);
            // }
            if (inventoryList.length) {
                for (const itemData of inventoryList) {
                    const [createdItem] = await actor.createEmbeddedDocuments('Item', [itemData]);
                    // Manipulate the created item as needed
                    console.log(`Added ${createdItem.name} to ${actor.name}.`);
                    if (itemData?.actions) {
                        createdItem.actionGroups = Actions.makeActionsGroup(createdItem, itemData.actions);
                        // createdItem.actionGroups = game.ars.ARSAction.convertFromActionBundle(createdItem, createdItem.actions);
                        await game.ars.ARSActionGroup.saveAll(createdItem);
                    }
                    // Example: Update the item with additional data
                    // await createdItem.update({ flags: { customFlag: 'exampleValue' } });
                }
            }

            this.context.maps.npcs.set(npcNode.nodeName, actor);

            if (!this.importData.actorImported) this.importData.actorImported = [];
            // importData.actorImported.push({ nodeId: npcNode.nodeName, type: 'npc', pack: pack.collection, name: actor.name, id: actor.id, img: actor.img })
            this.importData.actorImported.push({
                nodeId: npcNode.nodeName,
                type: 'npc',
                name: actor.name,
                pack: this.exportPack ? this.actorPack.collection : null,
                id: actor.id,
                img: actor.img,
            });
        }

        // console.log("actorManager.js import actor ==========>", actor);
        // console.log(`Done importing ${npcName} into ${pack.collection}`);
    }

    /**
     *
     * Process npcNode/XML data.
     *
     * @param {*} npcNode
     * @returns
     */
    processNPC(npcNode) {
        /**
         *
         * Create powers/actions datastructure
         *
         * @param {*} npcNode
         * @returns
         */
        function processPowers(npcNode) {
            let actionBundle = Actions.processWeaponList(npcNode); // get weapon actionBundle before starting.
            Actions.processPowers('powers', npcNode, actionBundle, this);
            return actionBundle;
        }

        /**
         *
         * Create abilityNotes data structure
         *
         * @param {*} npcNode
         */
        function processAbilityNotes(npcNode) {
            let notes = [];
            for (const noteNode of xpath.queryXPathAll(npcNode, 'abilitynoteslist/*')) {
                // console.log("parseXml processAbilityNodes noteNode==>", noteNode, noteNode.nodeName);
                let note = {
                    name: xpath.getNodeValue(noteNode, 'name'),
                    // text: Utils.reformatMarkup(xpath.getNodeValue(noteNode, 'text'), importData, 500),
                    text: xpath.getNodeValue(noteNode, 'text'),
                };
                notes.push(note);
            }

            return notes;
        }

        /**
         *
         * Create save data structure
         *
         * @param {*} savesNode
         * @returns
         */
        function processSaves(npcNode) {
            let saves = xpath.queryXPath(npcNode, 'saves');
            let npcData = {
                paralyzation: {
                    value: xpath.getNodeValue(saves, 'paralyzation/base', 20),
                },
                poison: {
                    value: xpath.getNodeValue(saves, 'poison/base', 20),
                },
                death: {
                    value: xpath.getNodeValue(saves, 'death/base', 20),
                },
                rod: {
                    value: xpath.getNodeValue(saves, 'rod/base', 20),
                },
                staff: {
                    value: xpath.getNodeValue(saves, 'staff/base', 20),
                },
                wand: {
                    value: xpath.getNodeValue(saves, 'wand/base', 20),
                },
                petrification: {
                    value: xpath.getNodeValue(saves, 'petrification/base', 20),
                },
                polymorph: {
                    value: xpath.getNodeValue(saves, 'polymorph/base', 20),
                },
                breath: {
                    value: xpath.getNodeValue(saves, 'breath/base', 20),
                },
                spell: {
                    value: xpath.getNodeValue(saves, 'spell/base', 20),
                },
            };

            return npcData;
        }
        /**
         * Create attributes data structure
         *
         * @param {*} npcNode
         * @returns
         */
        function processAttributes(npcNode) {
            let npcData = {
                size: getSizeType(xpath.getNodeValue(npcNode, 'size', 'medium')),

                ac: {
                    value: xpath.getNodeValue(npcNode, 'ac', 10),
                },
                thaco: {
                    value: xpath.getNodeValue(npcNode, 'thaco', 20),
                },
                hp: {
                    value: xpath.getNodeValue(npcNode, 'hp', 0),
                },
                movement: {
                    value: xpath.getNodeValue(npcNode, 'speed', 12),
                },
            };

            return npcData;
        }

        function getSizeType(size) {
            let sizeType = 'medium';
            if (size.match(/^t(iny)?/i)) sizeType = 'tiny';
            else if (size.match(/^s(m)?(mall)?/i)) sizeType = 'small';
            else if (size.match(/^m(edium)?/i)) sizeType = 'medium';
            else if (size.match(/^l(arge)?/i)) sizeType = 'large';
            else if (size.match(/^h(uge)?/i)) sizeType = 'huge';
            else if (size.match(/^g(argantuan)?/i)) sizeType = 'gargantuan';
            else sizeType = 'medium';
            return sizeType;
        }

        /**
         *
         * Return correct tag for alignment
         *
         * @param {String} align
         * @returns
         */
        function getAlignmentType(align) {
            let alignType = 'n';
            // OSRIC.alignmentTypes = {
            //     "n": "OSRIC.alignmentTypes.n",
            //     "ng": "OSRIC.alignmentTypes.ng",
            //     "ne": "OSRIC.alignmentTypes.ne",
            //     "cn": "OSRIC.alignmentTypes.cn",
            //     "cg": "OSRIC.alignmentTypes.cg",
            //     "ce": "OSRIC.alignmentTypes.ce",
            //     "ln": "OSRIC.alignmentTypes.ln",
            //     "lg": "OSRIC.alignmentTypes.lg",
            //     "le": "OSRIC.alignmentTypes.le"
            // };

            if (align.match(/^(ng|neutral good)/i)) alignType = 'ng';
            else if (align.match(/^(n(eutral)?$|true neutral)/i)) alignType = 'n';
            else if (align.match(/^(ne|neutral evil)/i)) alignType = 'ne';
            else if (align.match(/^(cn|chaotic neutral)/i)) alignType = 'cn';
            else if (align.match(/^(cg|chaotic good)/i)) alignType = 'cg';
            else if (align.match(/^(ce|chaotic evil)/i)) alignType = 'ce';
            else if (align.match(/^(ln|lawful neutral)/i)) alignType = 'ln';
            else if (align.match(/^(lg|lawful good)/i)) alignType = 'lg';
            else if (align.match(/^(le|lawful evil)/i)) alignType = 'le';
            else alignType = 'n';

            return alignType;
        }

        /**
         * create the ability data structure cha.value, str.value/etc
         *
         * @param {*} abilityNode
         * @returns
         */
        function processAbility(npcNode) {
            let abilities = xpath.queryXPath(npcNode, 'abilities');
            console.log('parseXml processAbility abilities-->', abilities);
            let npcData = {
                cha: {
                    value: xpath.getNodeValue(abilities, 'charisma/base', 10),
                },
                str: {
                    value: xpath.getNodeValue(abilities, 'strength/base', 10),
                },
                int: {
                    value: xpath.getNodeValue(abilities, 'intelligence/base', 10),
                },
                dex: {
                    value: xpath.getNodeValue(abilities, 'dexterity/base', 10),
                },
                con: {
                    value: xpath.getNodeValue(abilities, 'constitution/base', 10),
                },
                wis: {
                    value: xpath.getNodeValue(abilities, 'wisdom/base', 10),
                },
            };
            return npcData;
        }

        function processSpellInfo(npcNode) {
            let spellInfo = {
                level: {
                    arcane: {
                        value: parseInt(xpath.getNodeValue(npcNode, 'arcane/totalLevel', 0)),
                    },
                    divine: {
                        value: parseInt(xpath.getNodeValue(npcNode, 'divine/totalLevel', 0)),
                    },
                },
                slots: {
                    arcane: {
                        value: [
                            0,
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/spellslots1/max', 0)),
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/spellslots2/max', 0)),
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/spellslots3/max', 0)),
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/spellslots4/max', 0)),
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/spellslots5/max', 0)),
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/spellslots6/max', 0)),
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/spellslots7/max', 0)),
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/spellslots8/max', 0)),
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/spellslots9/max', 0)),
                        ],
                        bonus: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    },
                    divine: {
                        value: [
                            0,
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/pactmagicslots1/max', 0)),
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/pactmagicslots2/max', 0)),
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/pactmagicslots3/max', 0)),
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/pactmagicslots4/max', 0)),
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/pactmagicslots5/max', 0)),
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/pactmagicslots6/max', 0)),
                            parseInt(xpath.getNodeValue(npcNode, 'powermeta/pactmagicslots7/max', 0)),
                        ],
                        bonus: [0, 0, 0, 0, 0, 0, 0, 0],
                    },
                },
            };
            return spellInfo;
        }

        const _magicpotency = (text) => {
            let mth = 0;
            if (text) {
                const match = text.match(/\+(\d+) (weapon[s]?\s)?(or better|to hit)/i);
                if (match && match[0] && match[1]) {
                    console.log('TEXT required', text);
                    mth = parseInt(match[1]);
                }
            }
            return mth;
        };

        const _getToken = (text) => {
            let path = '';
            const match = text.match(/^(.*)(@.*)?$/i);
            if (match && match[1]) {
                // strip out the campaign/ piece if present
                const imgpath = match[1].replace('campaign/', '');
                // path = `worlds/${game.world.system}/${match[1]}`;
                path = `${this.importData.importPath}/${this.importData.importName}/${imgpath}`;
            }
            return path;
        };
        // console.log("parseXml processNPC querySelector-->", npcNode);
        // console.log("parseXml processNPC queryXPath-->3", xpath.queryXPath(npcNode, 'name'));
        // console.log("parseXml processNPC querySelector-->4", npcNode.getElementsByTagName(">name")[0]);
        const actionsOld = processPowers.bind(this)(npcNode);

        let npcData = {
            name: xpath.getNodeValue(npcNode, 'name'),
            img: _getToken(xpath.getNodeValue(npcNode, 'token')),
            activity: xpath.getNodeValue(npcNode, 'activity'),
            acNote: xpath.getNodeValue(npcNode, 'ac_text'),
            climate: xpath.getNodeValue(npcNode, 'climate'),
            diet: xpath.getNodeValue(npcNode, 'diet'),
            damage: xpath.getNodeValue(npcNode, 'damage'),
            frequency: xpath.getNodeValue(npcNode, 'frequency'),
            hitdice: xpath.getNodeValue(npcNode, 'hitDice', 1),
            hpCalculation: xpath.getNodeValue(npcNode, 'hd'),
            intelligence: xpath.getNodeValue(npcNode, 'intelligence_text'),
            inlair: xpath.getNodeValue(npcNode, 'inlair'),
            morale: xpath.getNodeValue(npcNode, 'morale'),
            movement: xpath.getNodeValue(npcNode, 'speed'),
            numberAppearing: xpath.getNodeValue(npcNode, 'numberappearing'),
            numberAttacks: xpath.getNodeValue(npcNode, 'numberattacks'),
            organization: xpath.getNodeValue(npcNode, 'organization'),
            specialAttacks: xpath.getNodeValue(npcNode, 'specialAttacks'),
            specialDefenses: xpath.getNodeValue(npcNode, 'specialDefense'),
            sizenote: xpath.getNodeValue(npcNode, 'size'),
            treasureType: xpath.getNodeValue(npcNode, 'treasure'),
            magicresist: xpath.getNodeValue(npcNode, 'magicresistance'),

            details: {
                type: xpath.getNodeValue(npcNode, 'type'),
                source: xpath.getNodeValue(npcNode, 'source'),
                alignment: getAlignmentType(xpath.getNodeValue(npcNode, 'alignment')),
                biography: {
                    // value: Utils.reformatMarkup(xpath.getNodeValue(npcNode, 'text'), importData, 200),
                    value: xpath.getNodeValue(npcNode, 'text'),
                },
            },

            resistances: {
                weapon: {
                    magicpotency: _magicpotency(xpath.getNodeValue(npcNode, 'specialDefense')),
                },
            },

            xp: {
                value: parseInt(xpath.getNodeValue(npcNode, 'xp', 0)),
            },

            attributes: processAttributes.bind(this)(npcNode),
            abilities: processAbility.bind(this)(npcNode),
            saves: processSaves.bind(this)(npcNode),
            abilityNotes: processAbilityNotes.bind(this)(npcNode),
            // actions: processPowers(npcNode)
            actions: actionsOld,
            spellInfo: processSpellInfo.bind(this)(npcNode),
        };

        return npcData;
    }
} // end class Actors
