import XmlImporter from './xml-import.js';
import * as xpath from './xpath.js';
import { Effects } from './effectsManager.js';
import { Utils } from './utilities.js';

export class Journals {
    constructor(node, folder, context, options = {}) {
        this.context = context;
        this.importData = context.importData;
        this.node = node;
        this.folder = folder;
        this.options = options;
    }
    async import(node, sortNum) {
        const name = xpath.getNodeValue(node, 'name', 'UNKNOWN');
        const text = Utils.reformatMarkup(xpath.getNodeValue(node, 'text'), this.context, 600);

        let journal = await JournalEntry.create(
            {
                // console.log({
                name: Utils.cleanSortingName(name),
                content: text,
                folder: this.folder.id,
                sort: sortNum,
            },
            { displaySheet: false }
        );
        this.context.maps.journals.set(node.nodeName, journal);
        // let journalCollection = await JournalEntry.create({
        //     name: `${tagData.chapter}.${tagData.sub} ${Utils.cleanSortingName(name)}`,
        //     content: text,
        //     sort: sortNum,
        // }, { displaySheet: false, pack: pack.collection });

        // console.log(`Done importing ${journal.name} into ${pack.collection}`);
        if (!this.importData.imported['journalentry']) this.importData.imported['journalentry'] = [];
        this.importData.imported['journalentry'].push({
            nodeId: node.nodeName,
            type: 'journalentry',
            name: journal.name,
            id: journal.id,
            img: journal.img,
        });
    }
} // end class Journals
