import * as xpath from './xpath.js';
import { Effects } from './effectsManager.js';
import { Utils } from './utilities.js';

export class Tables {
    /**
     *
     * @param {*} node
     * @param {*} exportPack
     * @param {*} pack
     * @param {*} folder
     * @param {*} context
     * @param {*} options
     */
    constructor(node, exportPack, pack, folder, context, options = {}) {
        this.context = context;
        this.importData = context.importData;
        this.node = node;
        this.folder = folder;
        this.exportPack = exportPack;
        this.pack = pack;
        this.options = options;
    }

    /**
     *
     * @param {*} node
     * @param {*} sortNum
     */
    async import(node, sortNum) {
        //NOTE This is for a specific situation where someone had bad ids and don't match
        // in this case we'll assume the table entry name is accurate
        /** IN ALL 99.9% of CASES THIS SHOULD BE FALSE! */
        //BUG: SO THIS IS HIGHLIGHTED!
        const matchOnNameOnly = false;
        //
        function processTablesResults(nodeRows, formula) {
            let results = [];
            let startRoll = 1;
            let lastRoll = 1;
            let rowList = xpath.getNodeList(nodeRows, 'tablerows/*');
            for (const nodeRow of rowList) {
                let lowRange = parseInt(xpath.getNodeValue(nodeRow, 'fromrange', '0'));
                let hiRange = parseInt(xpath.getNodeValue(nodeRow, 'torange', '0'));
                if (!lowRange) lowRange = lastRoll;
                if (startRoll > lowRange) startRoll = lowRange;
                if (lastRoll < startRoll) lastRoll = startRoll;
                if (!hiRange) hiRange = lowRange;
                if (lastRoll < hiRange) lastRoll = hiRange;

                let resultList = xpath.getNodeList(nodeRow, 'results/*');
                for (const nodeResult of resultList) {
                    let resultId = undefined;
                    let resultType = 0;
                    let collection;
                    let resultName = xpath.getNodeValue(nodeResult, 'result');
                    // if the entire name is wrapped in []'s remove them entirely.
                    // if (resultName.startsWith('\[') && resultName.endsWith('\]')) {
                    //     resultName = resultName.substring(1, resultName.length - 1)
                    // }
                    resultName = resultName.replace(/\[/, '[[');
                    resultName = resultName.replace(new RegExp(/\]/, 'i'), ']]');
                    const resultClass = xpath.getNodeValue(nodeResult, 'resultlink/class');
                    const resultRange = [lowRange, hiRange];
                    let idNode;
                    // console.log("tableManager.js processTablesResults", resultRange, lowRange, hiRange)
                    if (resultClass) {
                        // resultType = 2;
                        const rawRresultRecordId = xpath.getNodeValue(nodeResult, 'resultlink/recordname');
                        let match = rawRresultRecordId.match(/\.?(id-\d+)/i);
                        const resultRecordId = match[1].trim();

                        // find recordName using recordType.

                        switch (resultClass) {
                            case 'npc':
                                resultType = this.exportPack ? 2 : 1;
                                collection = this.exportPack ? this.pack.collection : 'Actor';
                                idNode = Utils.getNodeById(resultRecordId, this.importData.npcNodeList);
                                resultId = this.context.maps.items.get(idNode?.nodeName)?.id;
                                if (idNode && this.exportPack)
                                    collection = Utils.getPackByNodeId(collection, idNode, this.importData.actorImported);
                                break;
                            case 'table':
                                resultType = this.exportPack ? 2 : 1;
                                collection = this.exportPack ? this.pack.collection : 'RollTable';
                                idNode = Utils.getNodeById(resultRecordId, this.importData.tableNodeList);
                                resultId = this.context.maps.items.get(idNode?.nodeName)?.id;
                                if (idNode && this.exportPack)
                                    collection = Utils.getPackByNodeId(collection, idNode, this.importData.tableImported);
                                break;
                            case 'power':
                            case 'spell':
                                resultType = this.exportPack ? 2 : 1;
                                collection = this.exportPack ? this.pack.collection : 'Item';
                                // resultName, idNode = _getRecordName(importData.spellNodeList, resultRecordId)
                                idNode = Utils.getNodeById(resultRecordId, this.importData.spellNodeList);
                                resultId = this.context.maps.items.get(idNode?.nodeName)?.id;
                                if (idNode && this.exportPack)
                                    collection = Utils.getPackByNodeId(collection, idNode, this.importData.itemImported);
                                break;
                            case 'item':
                                resultType = this.exportPack ? 2 : 1;
                                collection = this.exportPack ? this.pack.collection : 'Item';
                                idNode = Utils.getNodeById(resultRecordId, this.importData.itemNodeList);
                                if (idNode && this.exportPack)
                                    collection = Utils.getPackByNodeId(idNode, this.importData.itemImported);
                                resultName = idNode ? xpath.getNodeValue(idNode, 'name') : 'UNKNOWN';
                                const nodeType = xpath.getNodeValue(idNode, 'type').toLowerCase();
                                const nodeSubType = xpath.getNodeValue(idNode, 'subtype').toLowerCase();
                                const itemType = Utils.getItemType(nodeType, nodeSubType);
                                resultId = this.context.maps.items.get(idNode?.nodeName)?.id;
                                // console.log("parseXml processItems nodeTypes==>", { nodeType, nodeSubType, itemType });
                                // resultName = xpath.getNodeValue(Utils.getNodeById(resultRecordId, importData.itemNodeList), 'name');
                                console.log('tableManager.js item ==>', {
                                    idNode,
                                    resultName,
                                    nodeType,
                                    nodeSubType,
                                    itemType,
                                });
                                break;

                            case 'battle':
                            case 'battlerandom':
                                resultType = 1;
                                collection = 'Item';
                                const rec = Utils.getImportedRecord(resultRecordId, this.importData.imported['encounter']);
                                if (rec) {
                                    resultId = rec.id;
                                    resultName = rec.name;
                                }
                                break;

                            case 'encounter':
                                resultType = 1;
                                collection = 'JournalEntry';
                                const jRec = Utils.getImportedRecord(resultRecordId, this.importData.imported['journalentry']);
                                if (jRec) {
                                    resultId = jRec.id;
                                    resultName = jRec.name;
                                } else {
                                    ui.notifications.info(`tableManager.js Cannot import Story in Table due to sequencing`);
                                    console.log('tableManager.js Cannot import Story in Table due to sequencing');
                                }
                                break;

                            default:
                                ui.notifications.warn(
                                    `tableManager.js processTablesRows - UNSUPPORTED IMPORT TABLE RECORD LINK TYPE ${resultClass}`
                                );
                                console.log(
                                    'tableManager.js processTablesRows - UNSUPPORTED IMPORT TABLE RECORD TYPE',
                                    resultClass
                                );
                                break;
                        }
                        if (idNode) {
                            if (matchOnNameOnly) {
                                // we dont try and match inode names for this 1 off case
                            } else {
                                resultName = Utils.cleanSortingName(xpath.getNodeValue(idNode, 'name'));
                            }
                        }
                    }

                    if (resultName)
                        // console.log('tableManager.js ------------->', {
                        //     resultId,
                        //     resultName,
                        //     resultType,
                        //     collection,
                        //     idNode,
                        //     resultRange,
                        //     lowRange,
                        //     hiRange,
                        // });
                        results.push({
                            _id: foundry.utils.randomID(16),
                            img: resultType == 2 ? 'icons/skills/trades/academics-investigation-study-blue.webp' : '',
                            type: resultType,
                            text: resultName,
                            collection: typeof collection == 'string' ? collection : undefined,
                            resultId: resultId,
                            weight: 1,
                            range: resultRange,
                            rangeL: lowRange,
                            rangeH: hiRange,
                        });
                }
            }

            formula = `${startRoll}d${lastRoll}`;
            return [formula, results];
        }

        const name = xpath.getNodeValue(node, 'name', 'UNKNOWN');
        //const description = Utils.reformatMarkup(xpath.getNodeValue(node, 'description'), importData, 200);
        const description = xpath.getNodeValue(node, 'description');
        const [formula, results] = processTablesResults.bind(this)(node, '');

        let tableItem = await RollTable.create(
            {
                // name: name,
                name: Utils.cleanSortingName(name),
                description: description,
                folder: this.exportPack ? '' : this.folder.id,
                img: 'icons/skills/trades/construction-mason-bricklayer-red.webp',
                formula: formula,
                replacement: true,
                displayRoll: true,
                results: results,
            },
            {
                displaySheet: false,
                pack: this.exportPack ? this.pack.collection : null,
            }
        );

        if (tableItem) {
            this.context.maps.tables.set(node.nodeName, tableItem);

            if (!this.importData.tableImported) this.importData.tableImported = [];
            this.importData.tableImported.push({
                nodeId: node.nodeName,
                type: 'rolltable',
                name: tableItem.name,
                pack: this.exportPack ? this.pack.collection : null,
                id: tableItem.id,
                img: tableItem.img,
            });

            console.log(`Done importing ${tableItem.name}`);
        } else {
            console.error('Failed to import table:', { name, description });
        }
    }
} // end tables class
