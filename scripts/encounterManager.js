import * as xpath from './xpath.js';
import { Utils } from './utilities.js';

export class Encounters {
    /**
     *
     * @param {*} node
     * @param {*} folder
     * @param {*} context
     * @param {*} options
     */
    constructor(node, folder, context, options = {}) {
        this.context = context;
        this.importData = context.importData;
        this.node = node;
        this.folder = folder;
        this.options = options;
    }

    async processEncounterNPCs(nodeNpcs) {
        let encounterIdList = [];

        let nodeList = xpath.getNodeList(nodeNpcs, `npclist/*`);
        for (const nodeNPC of nodeList) {
            const record = xpath.getNodeValue(nodeNPC, 'link/recordname');
            let ac = parseInt(xpath.getNodeValue(nodeNPC, 'ac'));
            if (ac >= 11) ac = undefined;
            const count = parseInt(xpath.getNodeValue(nodeNPC, 'count', '1'));
            const expr = xpath.getNodeValue(nodeNPC, 'expr');
            const hp = parseInt(xpath.getNodeValue(nodeNPC, 'hp', '0'));
            if (record) {
                const match = record.match(/\.?(id-\d+)/);
                if (match && match[1]) {
                    const idname = match[1];

                    for (const r of this.importData.actorImported) {
                        //{ nodeId: npcNode.nodeName, type: 'npc', name: actor.name, id: actor.id, img: actor.img }
                        if (r.nodeId === idname) {
                            encounterIdList.push({
                                id: r.id,
                                img: r.img,
                                name: r.name,
                                hp: hp,
                                ac: ac,
                                count: expr ? expr : count,
                            });
                        }
                    }
                }
            }
        }

        // console.log(`Done importing ${nodeList.length} npcs for encounter.`);
        return encounterIdList;
    }

    async import(node, sortNumber) {
        const name = xpath.getNodeValue(node, 'name', 'UNKNOWN');
        const xp = parseInt(xpath.getNodeValue(node, 'exp', '0'));
        const encounterIdList = await this.processEncounterNPCs(node);

        let encounterItem = await Item.create(
            {
                type: 'encounter',
                name: Utils.cleanSortingName(name),
                // img: 'icons/creatures/abilities/bear-roar-bite-brown-green.webp',
                img: game.ars.config.icons.general.items['encounter'],
                folder: this.folder.id,
                sort: sortNumber,
                system: {
                    npcList: encounterIdList,
                    xp: xp,
                },
            },
            { displaySheet: false }
        );
        this.context.maps.encounters.set(node.nodeName, encounterItem);

        console.log(`Done importing ${encounterItem.name}`, { encounterItem });

        if (!this.importData.imported['encounter']) this.importData.imported['encounter'] = [];
        this.importData.imported['encounter'].push({
            nodeId: node.nodeName,
            type: 'encounter',
            name: encounterItem.name,
            id: encounterItem.id,
            img: encounterItem.img,
        });
    }
} // end class Encounters
