import * as xpath from './xpath.js';
import { Utils } from './utilities.js';

export class JournalManuals {
    constructor(folder, context, options = {}) {
        this.context = context;
        this.importData = context.importData;
        this.folder = folder;
        this.options = options;
    }

    /**
     *
     * @param {*} startNode /root/reference/refmanualindex
     * @returns
     */
    async import() {
        console.log('Journal Manual START --->');
        const folderData = {
            name: this.importData.importName,
            type: 'JournalEntry',
            sorting: 'm',
            sort: 0,
        };
        const journalFolder = await Folder.implementation.create(folderData);

        let nodeList = this.context.importData.chapterList;

        const journalMap = new Map();
        nodeList.sort(Utils.sortNodeByOrder);
        let sortNumber = 0;
        for (const chapterNode of nodeList) {
            sortNumber += 1 + CONST.SORT_INTEGER_DENSITY;
            const chapterName = xpath.getNodeValue(chapterNode, 'name');
            const chapterSortOrder = parseInt(xpath.getNodeValue(chapterNode, 'order', '0')) + CONST.SORT_INTEGER_DENSITY;
            console.log(`Chapter Name: ${chapterName}`, nodeList);

            // Create a journal entry for the chapter
            let chapterJournal = await JournalEntry.create({
                name: chapterName ?? 'UNNAMED-CHAPTER',
                content: '',
                folder: journalFolder.id,
                // sort: chapterSortOrder,
                sort: chapterSortOrder,
                // other relevant journal entry properties
            });
            journalMap.set(foundry.utils.randomID(16), chapterJournal);

            const subChapterList = xpath.getNodeList(chapterNode, 'subchapters/*');
            subChapterList.sort(Utils.sortNodeByOrder);
            for (const subChapterNode of subChapterList) {
                const subChapterName = xpath.getNodeValue(subChapterNode, 'name');
                const subChapterSortOrder =
                    parseInt(xpath.getNodeValue(subChapterNode, 'order', '0')) + CONST.SORT_INTEGER_DENSITY;
                console.log(`Sub-Chapter Name: ${subChapterName}`);
                sortNumber += 1 + CONST.SORT_INTEGER_DENSITY;
                let subChapterPage = await JournalEntryPage.create(
                    {
                        name: subChapterName ?? 'UNNAMED-SUBCHAPTER',
                        title: {
                            level: 1,
                        },
                        parent: chapterJournal.id, // ID of the parent journal entry
                        sort: sortNumber,
                        // sort: subChapterSortOrder,
                        // other properties as needed
                    },
                    { parent: chapterJournal }
                );
                // journalMap.set(foundry.utils.randomID(16), subChapterPage);
                const refPagesList = xpath.getNodeList(subChapterNode, 'refpages/*');
                refPagesList.sort(Utils.sortNodeByOrder);
                for (const refPageNode of refPagesList) {
                    const pageName = xpath.getNodeValue(refPageNode, 'name');
                    // const pageListLinkNode = xpath.getNodeValue(refPageNode, 'listlink');

                    // Retrieve the 'listlink' node using an XPath expression
                    const pageListLinkNode = xpath.queryXPath(refPageNode, 'listlink');

                    // Check if the 'listlink' node exists and retrieve its 'type' attribute
                    let listAttribute = '';
                    if (pageListLinkNode) {
                        listAttribute = pageListLinkNode.getAttribute('type');
                    }

                    /**
                     *
                     * @param {*} nodeBlockList
                     */
                    async function blocksHelper(nodeBlockList) {
                        // go through each info block here
                        if (nodeBlockList) {
                            nodeBlockList.sort(Utils.sortNodeByOrder);

                            let pageText = '';
                            for (const blockNode of nodeBlockList) {
                                const blockType = xpath.getNodeValue(blockNode, 'blocktype');

                                if (blockType && blockType.includes('text')) {
                                    // Append block content to the page journal
                                    pageText += xpath.getNodeValue(blockNode, 'text');
                                }
                                if (blockType && blockType.includes('image')) {
                                    const caption = xpath.getNodeValue(blockNode, 'caption');
                                    const sizeString = xpath.getNodeValue(blockNode, 'size');
                                    const [height, width] = sizeString ? sizeString.split(',') : [0, 0];
                                    let imageURL = xpath.getNodeValue(blockNode, 'image/layers/layer/bitmap');
                                    imageURL = Utils.stripAfterAt(imageURL);
                                    imageURL = imageURL.replace(/campaign\//g, '');
                                    // const [height, width] = block?.size?.split(',') || [,];
                                    // `worlds/${game.world.id}/${tokenImage}`
                                    // pageText += `<img src="worlds/${game.world.id}/${imageURL}" alt="${caption}" height="${
                                    //     height ? height : ''
                                    // }" />`;
                                    // dropped any size, foundry fits to page
                                    pageText += `<img src="worlds/${game.world.id}/${imageURL}" alt="${caption}"}" />`;
                                }
                            } // end blocks

                            pageText = Utils.reformatMarkup(pageText, this.context, 200);
                            if (pageName && pageName != subChapterName) {
                                // Create a sub-page within the journal entry
                                sortNumber += 1 + CONST.SORT_INTEGER_DENSITY;
                                try {
                                    let pageJournal = await JournalEntryPage.create(
                                        {
                                            name: pageName ?? 'UNNAMED-PAGE',
                                            type: 'text',
                                            title: {
                                                level: 2,
                                            },
                                            text: {
                                                content: pageText,
                                            },
                                            sort: sortNumber,
                                            // other properties, and parent: subChapterJournal.id for hierarchy
                                            parent: chapterJournal.id, // ID of the parent journal entry
                                            // other properties as needed
                                        },
                                        { parent: chapterJournal }
                                    );
                                    // journalMap.set(foundry.utils.randomID(16), pageJournal);
                                } catch (err) {}
                            } else if (pageName) {
                                await subChapterPage.update({ 'text.content': pageText });
                            }
                        }
                    }

                    if (pageListLinkNode) {
                        const listAttribute = pageListLinkNode.getAttribute('type');
                        const pageBlocksList = xpath.getNodeList(refPageNode, 'blocks/*');
                        // console.log('pageBlocksList=>', typeof pageBlocksList);
                        if (pageBlocksList && pageBlocksList.length) {
                            blocksHelper.bind(this)(pageBlocksList);
                        } else if (listAttribute) {
                            const recordName = xpath.getNodeValue(pageListLinkNode, 'recordname');
                            let recordPath = Utils.stripAfterAt(recordName);
                            recordPath = recordPath.replace(/\./g, '/');
                            //reference.refmanualdata
                            const nodeBlocksList = xpath.getNodeList(this.context.data, `/root/${recordPath}/blocks/*`);
                            blocksHelper.bind(this)(nodeBlocksList);
                        }
                    }
                } //end pages
            } // end subchapter
        } // end chapter

        return journalMap;
    }
}
